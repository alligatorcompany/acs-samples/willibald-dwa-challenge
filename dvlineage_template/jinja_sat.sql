
with 
{% for source in cbc.context -%}
    {{ '\n' if not loop.first }}{{source.rel_name}} as (
  select  
    {# Sat HK #}
    {%- for attribute in source.attributes -%}
    {% set list = attribute.name.split(', ') %}
    {%- set alias = attribute.alias %}
    {%- if alias != None %}{% raw %}{{{% endraw %} dbt_utils.generate_surrogate_key([{%- endif -%}
      {%- for item in list -%}
         {{ ', ' if not loop.first }}
         {%- if alias != None %}{% raw %}"'{% endraw %}{{ item }}{% raw %}'"{% endraw %}{% else %}"{{ item }}"{% endif -%}
      {%- endfor -%}  
    {%- if alias != None %}]){% raw %}}}{% endraw %} as {{ alias }}{%- endif -%}{{',\n    ' if not loop.last }}  
    {%- endfor %}
  from {% raw %}{{{% endraw %} ref('{{source.rel_name}}') {% raw %}}}{% endraw %} 
){{',' if not loop.last }}
{%- endfor -%}

{% for source in cbc.context %}
select * from {{source.rel_name}}
{{' union \n' if not loop.last }}
{%- endfor %}
