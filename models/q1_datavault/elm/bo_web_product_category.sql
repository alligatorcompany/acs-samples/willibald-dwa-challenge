SELECT
    {{ dbt_utils.star(from=source('businessobjects','product_category_s_web')) }}
{% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_PRODUCT_CATEGORY_S_DVB_HUB') }}
{% else %}
FROM {{ source('businessobjects','product_category_s_web') }}
{% endif %}
WHERE FV_PRODUCT_CATEGORY = true
