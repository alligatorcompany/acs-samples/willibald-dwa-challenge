with
    href_on_time_delivery as (
        select *
        from {{ ref('bo_ref_href_on_time_delivery') }}
    ),

    on_time_delivery as (
        select
            on_time_delivery_h,
            "Anzahl Tage von" as numberofdaysfrom,  -- noqa: RF05
            "Anzahl Tage bis" as numberofdaysuntil, -- noqa: RF05
            "Bezeichnung" as designation,
            "Bewertung" as evaluation
        from href_on_time_delivery
    ),

    on_time_delivery_final as (
        select
            on_time_delivery_h,
            numberofdaysfrom,
            numberofdaysuntil,
            designation,
            evaluation,
            case
                when
                    numberofdaysfrom != 'xxx'
                    and numberofdaysfrom != 'zzz'
                    and numberofdaysfrom != 'yyy'
                    then cast(numberofdaysfrom as varchar(200)) || '|' || cast(numberofdaysuntil as varchar(200))
                else numberofdaysfrom
            end as range_indicator
        from on_time_delivery
    )

select *
from on_time_delivery_final
order by range_indicator asc
