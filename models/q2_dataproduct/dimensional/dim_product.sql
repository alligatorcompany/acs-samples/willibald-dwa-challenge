with
    product as ( -- noqa: PRS
        select
            {{ dbt_utils.star(ref('bo_web_product')) }}
        from {{ ref('bo_web_product') }}
    ),

    product_category as (
        select
            {{ dbt_utils.star(ref('bo_web_product_category')) }}
        from {{ ref('bo_web_product_category') }}
    )

select
    product.product_h,
    product.produktid,
    product.katid,
    product.bezeichnung,
    product.umfang,
    product.typ,
    product.preis,
    product.pflanzort,
    product.pflanzabstand,
    product_category.product_category_h,
    product_category.oberkatid,
    product_category.name
from product
inner join product_category
    on product.katid = product_category.katid
