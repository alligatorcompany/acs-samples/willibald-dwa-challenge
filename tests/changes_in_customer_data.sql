--Erst mit der dritten und letzten Periode relevant
with
    union_query as (
        select
            kundeid,
            vorname,
            name,
            geschlecht,
            1 as periode
        from {{ source('webshop_periode_1','kunde_periode_1') }}
        union all
        select
            kundeid,
            vorname,
            name,
            geschlecht,
            2 as periode
        from {{ source('webshop_periode_2','kunde_periode_2') }}
        union all
        select
            kundeid,
            vorname,
            name,
            geschlecht,
            3 as periode
        from {{ source('webshop_periode_3','kunde_periode_3') }}
    )
    ,

    src as (
        select
            a.kundeid,
            a.vorname,
            a.name,
            a.geschlecht,
            a.periode
        from (
            select
                kundeid,
                vorname,
                name,
                geschlecht,
                periode,
                lead(geschlecht, 1)
                    over (partition by kundeid order by periode)
                    as prev
            from
                union_query
            order by kundeid asc, periode asc
        ) as a
        where
            a.prev != a.geschlecht
        union
        select
            kundeid,
            vorname,
            name,
            geschlecht,
            periode
        from (
            select
                kundeid,
                vorname,
                name,
                geschlecht,
                periode,
                lag(geschlecht, 1)
                    over (partition by kundeid order by periode)
                    as prev
            from
                union_query
            order by kundeid asc, periode asc
        ) as a
        where
            a.prev != geschlecht
    ),

    dv as (
        select
            a.kundeid,
            a.vorname,
            a.name,
            a.geschlecht,
            a.periode
        from (
            select
                kundeid,
                vorname,
                name,
                geschlecht,
                periode,
                lead(geschlecht, 1)
                    over (partition by kundeid order by periode)
                    as prev
            from
                {{ ref('bo_web_customer_period') }}
            order by kundeid asc, periode asc
        ) as a
        where
            a.prev != a.geschlecht
        union
        select
            kundeid,
            vorname,
            name,
            geschlecht,
            periode
        from (
            select
                kundeid,
                vorname,
                name,
                geschlecht,
                periode,
                lag(geschlecht, 1)
                    over (partition by kundeid order by periode)
                    as prev
            from
                {{ ref('bo_web_customer_period') }}
            order by kundeid asc, periode asc
        ) as a
        where
            a.prev != geschlecht
    ),

    src_minus_dv as (

        select kundeid from src
        except
        select kundeid from dv

    ),

    dv_minus_src as (

        select kundeid from dv
        except
        select kundeid from src

    ),

    unioned as (

        select
            'src_minus_dv' as which_diff,
            src_minus_dv.*
        from src_minus_dv
        union all
        select
            'dv_minus_src' as which_diff,
            dv_minus_src.*
        from dv_minus_src

    )

select * from unioned
