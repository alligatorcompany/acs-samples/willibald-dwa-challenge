version: 2

models:
  - name: stg_href_on_time_delivery
    description: |
      Staging View containing data to determine whether a delivery was arriving to early, on time or to late
    columns:
      - name: anzahl tage von
        data_type: varchar
        description: |
          Number of days from which a delivery is within a defined status of an on time delivery
      - name: anzahl tage bis
        data_type: varchar
        description: |
          Number of days until which a delivery is within a defined status of an on time delivery
      - name: bezeichnung
        data_type: varchar
        description: |
          Description for classifying deliveries
      - name: bewertung
        data_type: varchar
        description: |
          Description for ranking the performance of the delivery

  - name: stg_ref_product_type
    description: |
      Staging View containing data of all available product typs
    columns:
      - name: typ
        data_type: varchar
        description: |
          Unique identifier for the type of product
      - name: bezeichnung
        data_type: varchar
        description: |
          Full Name of the product

  - name: stg_rs_order
    description: |
      Staging View containing data for all placed orders during the roadshow
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
      - name: kundeid
        data_type: number
        description: |
          Unique identifier of a customer
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
      - name: kaufdatum
        data_type: varchar
        description: |
          Date on which the product was bought
      - name: kreditkarte
        data_type: varchar
        description: |
          Credit card number
      - name: gueltigbis
        data_type: varchar
        description: |
          Month and Year to which the Credit card is valid
      - name: kkfirma
        data_type: varchar
        description: |
          Name of Credit card company who issued the credit card
      - name: produktid
        data_type: number
        description: |
          Unique identifier of a product
      - name: menge
        data_type: number
        description: |
          Number of items ordered
      - name: preis
        data_type: number
        description: |
          Price of the product
      - name: rabatt
        data_type: number
        description: |
          Discount on price in percentage

  - name: stg_web_address_residence
    description: |
      Staging View containing data of the address of residence for each customer
    columns:
      - name: kundeid
        data_type: number
        description: |
          Unique identifier of a customer
      - name: von
        data_type: varchar
        description: |
          Start date address
      - name: bis
        data_type: varchar
        description: |
          End date address
      - name: strasse
        data_type: varchar
        description: |
          Street Name
      - name: hausnummer
        data_type: varchar
        description: |
          House number
      - name: plz
        data_type: number
        description: |
          Zip Code of residence
      - name: ort
        data_type: varchar
        description: |
          City of residence
      - name: land
        data_type: number
        description: |
          Country of residence
      - name: adresszusatz
        data_type: varchar
        description: |
          Address supplement

  - name: stg_web_club_partner
    description: |
      Staging View containing data of all club partners
    columns:
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
      - name: kundeidverein
        data_type: number
        description: |
          Unique identifier of customer representing a club partner
      - name: rabatt1
        data_type: number
        description: |
          Discount 1
      - name: rabatt2
        data_type: number
        description: |
          Discount 2
      - name: rabatt3
        data_type: number
        description: |
          Discount 3

  - name: stg_web_customer
    description: |
      Staging View containing data for all customers
    columns:
      - name: kundeid
        data_type: varchar
        description: |
          Unique identifier of a customer
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
      - name: vorname
        data_type: varchar
        description: |
          First Name of customer
      - name: name
        data_type: varchar
        description: |
          Last Name of customer
      - name: geschlecht
        data_type: varchar
        description: |
          Gender of customer
      - name: geburtsdatum
        data_type: varchar
        description: |
          Date of birth
      - name: telefon
        data_type: varchar
        description: |
          Telefon number
      - name: mobil
        data_type: varchar
        description: |
          Mobil telefon number
      - name: email
        data_type: varchar
        description: |
          Email Address
      - name: kreditkarte
        data_type: varchar
        description: |
          Credit card number
      - name: gueltigbis
        data_type: varchar
        description: |
          Month and Year to which the Credit card is valid
      - name: kkfirma
        data_type: varchar
        description: |
          Name of Credit card company who issued the credit card

  - name: stg_web_delivery_address
    description: |
      Staging View containing data for all available delivery addresses of each customers
    columns:
      - name: lieferadrid
        data_type: varchar
        description: |
          Unique identifier of a delivery address
      - name: kundeid
        data_type: varchar
        description: |
          Unique identifier of a customer
      - name: strasse
        data_type: varchar
        description: |
          Street Name
      - name: hausnummer
        data_type: varchar
        description: |
          House number
      - name: adresszusatz
        data_type: varchar
        description: |
          Address supplement
      - name: plz
        data_type: varchar
        description: |
          Zip Code of residence
      - name: ort
        data_type: varchar
        description: |
          City of residence
      - name: land
        data_type: number
        description: |
          Country of residence

  - name: stg_web_delivery_service
    description: |
      Staging View containing data for all available delivery services
    columns:
      - name: lieferdienstid
        data_type: varchar
        description: |
          Unique identifier of a delivery service
      - name: name
        data_type: varchar
        description: |
          Name of delivery service
      - name: telefon
        data_type: varchar
        description: |
          Telefon number
      - name: fax
        data_type: varchar
        description: |
          Fax number
      - name: email
        data_type: varchar
        description: |
          Email Address
      - name: strasse
        data_type: varchar
        description: |
          Street Name
      - name: hausnummer
        data_type: varchar
        description: |
          House number
      - name: plz
        data_type: varchar
        description: |
          Zip Code of residence
      - name: ort
        data_type: varchar
        description: |
          City of residence
      - name: land
        data_type: number
        description: |
          Country of residence

  - name: stg_web_delivery
    description: |
      Staging View containing data for each delivery of an order
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
      - name: posid
        data_type: number
        description: |
          Unique identifier of the order line item
      - name: lieferadrid
        data_type: number
        description: |
          Unique identifier of a delivery address
      - name: lieferdienstid
        data_type: number
        description: |
          Unique identifier of a delivery service
      - name: lieferdatum
        data_type: varchar
        description: |
          Date on which the product is scheduled to be delivered

  - name: stg_web_order
    description: |
      Staging View containing data for all placed orders in the webshop
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
      - name: kundeid
        data_type: number
        description: |
          Unique identifier of a customer
      - name: allglieferadrid
        data_type: number
        description: |
          Reference to LieferAdrID in delivery address
      - name: bestelldatum
        data_type: varchar
        description: |
          Date on which the order was placed
      - name: wunschdatum
        data_type: varchar
        description: |
          Date on which the customer whishes the order to be delivered
      - name: rabatt
        data_type: number
        description: |
          Discount on price in percentage

  - name: stg_web_position
    description: |
      Staging View containing data for each order line of an order
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
      - name: posid
        data_type: number
        description: |
          Unique identifier of the order line item
      - name: produktid
        data_type: number
        description: |
          Unique identifier of a product
      - name: spezlieferadrid
        data_type: number
        description: |
          Reference to LieferAdrID in delivery address
      - name: menge
        data_type: number
        description: |
          Number of items ordered
      - name: preis
        data_type: number
        description: |
          Price of the product

  - name: stg_web_product_category
    description: |
      Staging View containing data of all available product categories
    columns:
      - name: katid
        data_type: varchar
        description: |
          Unique identifier of a product category
      - name: oberkatid
        data_type: varchar
        description: |
          Unique identifier of a top product category
      - name: name
        data_type: varchar
        description: |
          Full Name of product category

  - name: stg_web_product
    description: |
      Staging View containing data for each available product
    columns:
      - name: produktid
        data_type: number
        description: |
          Unique identifier of a product
      - name: katid
        data_type: varchar
        description: |
          Unique identifier of a product category
      - name: bezeichnung
        data_type: varchar
        description: |
          Full Name of the product
      - name: umfang
        data_type: varchar
        description: |
          Description of size or scale of the product
      - name: typ
        data_type: number
        description: |
          Unique identifier for the type of product
      - name: preis
        data_type: number
        description: |
          Price of the product
      - name: pflanzort
        data_type: varchar
        description: |
          Description of where Plants should be planted
      - name: pflanzabstand
        data_type: varchar
        description: |
          Description of how Plants should be planted

  - name: stg_web_product_category_union
    description: |
      Staging View containing data of all available product categories unionized across all time periods
    columns:
      - name: katid
        data_type: varchar
        description: |
          Unique identifier of a product category
      - name: oberkatid
        data_type: varchar
        description: |
          Unique identifier of a top product category
      - name: name
        data_type: varchar
        description: |
          Full Name of the product
      - name: periode
        data_type: number
        description: |
          Specifies the Period of the which the record is part of

  - name: stg_web_order_union
    description: |
      Staging View containing data for all placed orders in the webshop unionized across all time periods
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
      - name: kundeid
        data_type: number
        description: |
          Unique identifier of a customer
      - name: allglieferadrid
        data_type: number
        description: |
          Reference to LieferAdrID in delivery address
      - name: bestelldatum
        data_type: varchar
        description: |
          Date on which the order was placed
      - name: wunschdatum
        data_type: varchar
        description: |
          Date on which the customer whishes the order to be delivered
      - name: rabatt
        data_type: number
        description: |
          Discount on price in percentage
      - name: periode
        data_type: number
        description: |
          Specifies the Period of the which the record is part of
      - name: delete_flag
        data_type: boolean
        description: |
          Specifies whether a record was deleted in the previous period
