SELECT
    {{ dbt_utils.star(from=source('businessobjects','ref_product_type_s_dvb_hub')) }}
{% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_REF_PRODUCT_TYPE_S_DVB_HUB') }}
{% else %}
FROM {{ source('businessobjects','ref_product_type_s_dvb_hub') }}
{% endif %}
WHERE FV_REF_PRODUCT_TYPE = true
