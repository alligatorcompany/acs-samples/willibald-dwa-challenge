#!/bin/bash
cd ../../Willibald-Data/Roadshow/Tag\ 1
cp *.csv /Users/niklaslorenz/DBT_Projekte/willibald_dwa_challenge/seeds_raw/seeds_roadshow_tag_1
cd ../Tag\ 2
cp *.csv /Users/niklaslorenz/DBT_Projekte/willibald_dwa_challenge/seeds_raw/seeds_roadshow_tag_2
cd ../Tag\ 3
cp *.csv /Users/niklaslorenz/DBT_Projekte/willibald_dwa_challenge/seeds_raw/seeds_roadshow_tag_3
cd ../../Webshop/Testdaten\ Periode\ 1
cp *.csv /Users/niklaslorenz/DBT_Projekte/willibald_dwa_challenge/seeds_raw/seeds_webshop_periode_1
cd ../Testdaten\ Periode\ 2
cp *.csv /Users/niklaslorenz/DBT_Projekte/willibald_dwa_challenge/seeds_raw/seeds_webshop_periode_2
cd ../Testdaten\ Periode\ 3
cp *.csv /Users/niklaslorenz/DBT_Projekte/willibald_dwa_challenge/seeds_raw/seeds_webshop_periode_3
cd /Users/niklaslorenz/DBT_Projekte/willibald_dwa_challenge/seeds_raw/seeds_webshop_periode_1
mv href_termintreue.csv ../seeds_href_termintreue_periode_1/
mv ref_produkt_typ.csv ../seeds_ref_produkt_typ/
cd ../seeds_webshop_periode_2
mv href_termintreue.csv ../seeds_href_termintreue_periode_2/
