{{ config (materialized='view') }}

select
    {{ dbt_utils.star(ref('stg_web_product_category')) }}
from {{ ref('stg_web_product_category') }}
