FROM nginx:alpine
ARG WORKDIR
COPY ${WORKDIR}/ /code
COPY ${WORKDIR}/target/ /usr/share/nginx/html/
WORKDIR /usr/share/nginx/html/

