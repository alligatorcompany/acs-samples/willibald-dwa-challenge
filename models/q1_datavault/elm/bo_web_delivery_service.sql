SELECT
    {{ dbt_utils.star(from=source('businessobjects','delivery_service_s_web')) }}
{% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_DELIVERY_SERVICE_S_WEB') }}
{% else %}
FROM {{ source('businessobjects','delivery_service_s_web') }}
{% endif %}
WHERE FV_DELIVERY_SERVICE = true
