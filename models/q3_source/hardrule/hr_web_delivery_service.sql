{{ config (materialized='view') }}

select
    {{ dbt_utils.star(ref('stg_web_delivery_service')) }}
from {{ ref('stg_web_delivery_service') }}
