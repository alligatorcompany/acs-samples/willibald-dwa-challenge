import csv
import glob
import json
import logging
import os
import pathlib
import re
import sys
from dataclasses import dataclass, field
from typing import Dict, List, Optional

from dataclasses_json import dataclass_json
from jinja2 import Environment, FileSystemLoader, select_autoescape
from sqlglot import exp, parse_one
from sqlglot.errors import (ErrorLevel, ParseError, concat_messages,
                            merge_errors)

# to create Staging Tables CREATE_STAGING = True
CREATE_STAGING = False # False 

# Business Rules SQL processing (RE or SQLGLOT Tools) 
BR_PROCESIING = ""  #"RE" # OUTHER SQLGLOT TOOLS
# used only by choosing SQLGLOT
SQLGLOT_DIALECT = "snowflake" # "exasol" unused

# TEMPLATE_PATH='./exasol/templates'
TEMPLATE_PATH_JINJA = "./dvlineage_template/"
HASHKEY_DATATYPE = "HASHTYPE(16 BYTE)"
HASHKEY_COMMENT = "Hash Key"
BK_DATATYPE = "VARCHAR(2000000) UTF8"
BK_COMMENT = "Business Key"
# TARGET_PATH='./target'
LOG_LEVEL = logging.INFO
LOG = logging.getLogger("dv_lineage")

# Hub Load are only "keys_are_unique"=true or all
HUBLOADUNIQUE = False   # True

@dataclass_json
@dataclass
class SourceSystem:
    deployable: bool
    hidden_in_system_deployment: bool
    source_type_id: str
    source_type_name: str
    source_type_url: str
    system_color: str
    system_comment: Optional[str]
    system_id: str
    system_name: str

@dataclass_json
@dataclass
class JdbcDataType:
    character_maximum_length: Optional[int]
    data_type_name: str
    jdbc_data_type_id: Optional[int]
    numeric_precision: Optional[int]
    numeric_scale: Optional[int]


@dataclass_json
@dataclass
class Column:
    column_id: str
    column_order: int
    column_name: Optional[str] = None
    complete_data_type: Optional[str] = None
    jdbc_data_type: Optional[JdbcDataType] = None
    column_comment: Optional[str] = None

@dataclass_json
@dataclass
class TransactionLinkRelation:
    linked_hub_id: str    

@dataclass_json
@dataclass
class StagingTable:
    system_id: str
    columns: List[Column]
    loading_batch_size: int
    source_schema_name: str
    source_schema_or_system_id: str
    source_table_id: str
    source_table_name: str
    source_table_staging_id: str
    source_table_type_id: str
    staging_table_comment: Optional[str]
    staging_table_display_string: str
    staging_table_id: str
    staging_table_name: str
    where_clause_delta_part_template: Optional[str]
    where_clause_general_part: Optional[str]
    source_system: Optional[SourceSystem] = None

@dataclass_json
@dataclass
class HubLoad:
    business_key_prefix: str
    business_key_short: str
    column_ids: List[Column]
    datavault_category_id: str
    hub_id: str
    hub_load_display_name: str
    hub_load_id: str
    keys_are_prehashed: bool
    keys_are_unique: bool
    staging_table_id: str
    system_id: str
    system_name: str
    source_system: Optional[SourceSystem] = None
    stage: Optional[StagingTable] = None

@dataclass_json
@dataclass
class LinkLoad:
    link_id: str
    link_load_display_name: str
    link_load_id: str
    staging_table_id: str
    system_id: str
    source_system: Optional[SourceSystem] = None
    stage: Optional[StagingTable] = None

@dataclass_json
@dataclass
class Satellite:
    column_ids: List[Column]
    functional_suffix_id: str
    functional_suffix_name: str
    hub_id: str
    satellite_comment: str
    satellite_id: str
    satellite_is_prototype: bool
    satellite_subject_area_name: str
    staging_resource_id: str
    staging_table_id: str
    system_id: str
    source_system: Optional[SourceSystem] = None
    stage: Optional[StagingTable] = None

@dataclass_json
@dataclass
class Hub:
    hub_comment: Optional[str]
    hub_id: str
    hub_id_of_alias_parent: Optional[str]
    hub_name: str
    hub_name_of_alias_parent: str
    hub_subject_area_name: str
    satellites: List[Satellite] = field(default_factory=list, init=False)
    loads: List[HubLoad] = field(default_factory=list, init=False)

@dataclass_json
@dataclass
class Link:
    hub_a_id: str
    hub_b_id: str
    link_comment: str
    link_id: str
    link_subject_area_name: str
    link_suffix_id: Optional[str]
    link_suffix_name: str
    link_type: str
    loads: List[LinkLoad] = field(default_factory=list, init=False)
    hub_a: Optional[Hub] = None
    hub_b: Optional[Hub] = None

@dataclass_json
@dataclass
class TransactionLink:
    column_ids: List[Column]
    functional_suffix_id: str
    functional_suffix_name: str
    hub_id: str
    staging_resource_id: str
    staging_table_id: str
    system_id: str
    transaction_link_comment: str
    transaction_link_id: str
    transaction_link_relations: List[TransactionLinkRelation]
    transaction_link_subject_area_name: str

@dataclass_json
@dataclass
class BusinessobjectSet:
    businessobject_set_display_name: str
    businessobject_set_id: str
    functional_suffix_id: str
    functional_suffix_name: str
    start_hub_id: str
    start_hub_name: str


@dataclass_json
@dataclass
class BusinessobjectElement:
    columns: List[Column]
    path_id: str


@dataclass_json
@dataclass
class DependentObject:
    object_id: str
    object_type: Optional[str] = None


@dataclass_json
@dataclass
class Businessobject:
    businessobject_comment: str
    businessobject_display_name: str
    businessobject_dropdown_entry_string: str
    businessobject_elements: List[BusinessobjectElement]
    businessobject_set_id: str
    businessobject_structure: Optional[str]
    businessobject_view_id: str
    dependent_objects: List[DependentObject]
    granularity_satellite_id: Optional[str]
    system_id: str
    system_name: str
    businessobjectsets: List[BusinessobjectSet] = field(
        default_factory=list, init=False
    )

@dataclass_json
@dataclass
class BusinessRule:
    accesslayer_priorization: int
    additional_deployment_parameters: Optional[str]
    alias_schema_id: Optional[str]
    alias_view_nq_id: Optional[str]
    business_rules_view_code: str
    business_ruleset_comment: Optional[str]
    business_ruleset_name: str
    business_ruleset_order: int
    business_ruleset_suffix_id: Optional[str]
    business_ruleset_view_id: str
    column_nq_ids: List[str]
    container_id: str
    dependent_objects: List[DependentObject]
    include_in_accesslayer: bool
    is_error_ruleset: bool
    is_unaltered_business_ruleset: bool
    quick_inserts: Optional[str]
    system_id: str

@dataclass
class DVB:
    sources: Dict[str, SourceSystem] = field(default_factory=dict, init=False)
    stages: Dict[str, StagingTable] = field(default_factory=dict, init=False)
    hubs: Dict[str, Hub] = field(default_factory=dict, init=False)
    satellites: Dict[str, Satellite] = field(default_factory=dict, init=False)
    links: Dict[str, Link] = field(default_factory=dict, init=False)
    transaction_links: Dict[str, TransactionLink] = field(default_factory=dict, init=False)
    dimensional_model: Dict[str, Businessobject] = field(
        default_factory=dict, init=False
    )
    business_rules: Dict[str, BusinessRule] = field(default_factory=dict, init=False)


def parse_dvb_json(
    dvb_meta_path: str,
) -> DVB:
    dv = DVB()
    json_files = glob.glob(dvb_meta_path + "/**/*.json", recursive=True)
    for json in json_files:
        jsonPath = pathlib.PurePath(json)
        type = jsonPath.parent.name
        match type:
            case "hubs":
                fs = open(jsonPath, "r")
                hub = Hub.from_json(fs.read())
                dv.hubs[hub.hub_id] = hub
            case "source_systems":
                fs = open(jsonPath, "r")
                source = SourceSystem.from_json(fs.read())
                dv.sources[source.system_id] = source
            case "dimensional_model":
                fs = open(jsonPath, "r")
                dm = Businessobject.from_json(fs.read())
                dv.dimensional_model[dm.businessobject_view_id.split(".")[1]] = dm
            case "business_rules":
                fs = open(jsonPath, "r")
                br = BusinessRule.from_json(fs.read())
                dv.business_rules[br.business_ruleset_view_id.split(".")[1]] = br

    for json in json_files:
        jsonPath = pathlib.PurePath(json)
        type = jsonPath.parent.name
        match type:
            case "staging_tables":
                stage = StagingTable.from_json(open(jsonPath, "r").read())
                stage.source_system = dv.sources[stage.system_id]
                dv.stages[stage.staging_table_id] = stage
            case "satellites":
                sat = Satellite.from_json(open(jsonPath, "r").read())
                sat.stage = dv.stages[sat.staging_table_id]
                sat.source_system = dv.sources[sat.system_id]
                dv.hubs[sat.hub_id].satellites.append(sat)
                dv.satellites[sat.satellite_id] = sat
            case "links":
                link = Link.from_json(open(jsonPath, "r").read())
                link.hub_a = dv.hubs[link.hub_a_id]
                link.hub_b = dv.hubs[link.hub_b_id]
                dv.links[link.link_id] = link
            case "transaction_links":
                transaction_link = TransactionLink.from_json(open(jsonPath, "r").read())
                transaction_link.hub_id = dv.hubs[transaction_link.hub_id]
                dv.transaction_links[transaction_link.transaction_link_id] = transaction_link   

    for json in json_files:
        jsonPath = pathlib.PurePath(json)
        type = jsonPath.parent.name
        match type:
            case "hub_loads":
                hubload = HubLoad.from_json(open(jsonPath, "r").read())
                hubload.stage = dv.stages[hubload.staging_table_id]
                hubload.source_system = dv.sources[hubload.system_id]
                hubload.hub = dv.hubs[hubload.hub_id]
                hubload.hub.loads.append(hubload)
            case "link_loads":
                linkload = LinkLoad.from_json(open(jsonPath, "r").read())
                linkload.stage = dv.stages[linkload.staging_table_id]
                linkload.source_system = dv.sources[linkload.system_id]
                linkload.link = dv.links[linkload.link_id]
                linkload.link.loads.append(linkload)
            case "businessobjects_groups":
                bogroup = BusinessobjectSet.from_json(open(jsonPath, "r").read())

    return dv


@dataclass
class Attribute:
    """
    jedes attribute wird zu einem SQL-Ausdruck in der projektionsliste des zugehörigen select
    a) sql_function as alias -- wenn sql_function und alias gefüllt sind
    b) name as alias -- wenn name and alias gefüllt sind und sql_funciton=None
    c) name - wenn alias=None
    sollte eine sql-function anwendung finden, dann muss auch zwingend ein alias definiert sein
    """

    name: str
    alias: str
    sql_function: str


@dataclass
class BusinessKey:
    """
    jeder bk wird zu einem cte
    CTE_NAME/key_name as (select [distinct/is_unique] (attributes) from rel_name)
    """

    key_name: str
    rel_name: str
    attributes: List[Attribute]
    is_unique: bool


@dataclass
class Context:
    """
    jeder context wird zu eine DBT modell (Satellit), der inkrementell geladen wird
    da wir nur die logische lineage abbilden benötigen wir hier kein scd2
    attributliste distinct slectieren
    +hashkey vom cbc key (BusinessKey)
    +hashdiff aus attributen für vergleich
    +ldts
    +attributes
    """

    context_name: str
    rel_name: str
    key: BusinessKey
    attributes: List[Attribute]


@dataclass
class CBC:
    """
    jede cbc wird zu einem dbt modell mit dem hub namen
    allen bks als cte
    und einem finalen cte aus dem union aller bk-ctes
    select * from bk1.key_name
    union
    select * from bk2.key_name
    ...
    """

    cbc_name: str
    keyset: List[BusinessKey] = field(default_factory=list, init=False)
    context: List[Context] = field(default_factory=list, init=False)


@dataclass
class NBRLoad:
    """
    jeder nbr-load wird zu einer cte
    alle bk müssen sich immer auf eine rel_name beziehen
    CTE_NAME/load_name as (select distinct link-hash,ldts,cbc1_hash(bk1-columns),cbc2_hash(bk2_cols),... from rel_name)

    die bks müssen sich alle auf denselben rel_name, also source/staging beziehen
    """

    load_name: str
    rel_name: str
    cbc_rel: List[BusinessKey]


@dataclass
class NBR:
    """
    jeder nbr wird zu einem DBT modell / link der mit mehreren cte im union beladen wird
    select * from nbrload1.load_name
    union
    select * from nbrload2.load_name
    ...

    jeder nbrload muss die identische bk-list struktur aufweisen (gleiche anzahl von keys)
    """

    nbr_name: str
    nbr_type: str  # n:m, 1:n, n:1
    loads: List[NBRLoad]

@dataclass
class TNBR:
    """
    Transaction Link
    
    """

    tlink_name: str
    tlink_load: str  
    hubs: List[str]
    bk_columns: List[List[str]]

@dataclass
class DocsColLoad:
    """
    Loads for BK Columns
    """

    load_name: str
    column_name: List[str]
    column_type: List[str]


@dataclass
class Docs:
    """
    Column Attributes Docs
    """

    file_name: str
    file_comment: str
    column_name: List[str]
    column_type: List[str]
    column_comment: List[str]
    bk_colunm_load: Optional[List[DocsColLoad]] = None


def bo_path(body, path):
    # This is a recursive function to find the path for businesobject
    path_str = path
    listelem = json.loads(json.dumps(body))
    if "hubs" in listelem:
        for lh in listelem["hubs"]:
            path_str = path_str + " --> " + lh["hub_id"]
        return bo_path(lh, path_str)

    if "links" in listelem:
        for ll in listelem["links"]:
            lkey = "link_id"
            if  lkey not in ll:
                lkey = "transaction_link_id"
            path_str = path_str + " --> " + ll[lkey]
        return bo_path(ll, path_str)

    if "satellites" in listelem:
        for ls in listelem["satellites"]:
            path_str = path_str + " --> " + ls["satellite_id"]
        return path_str

    return path_str


def main():
    """
    das staging benötigen wir nicht, da nur die logische abbildung der quellen auf die hub/link/satellit
    benötigt werden.

    - zugriffe auf die rel_name Relationen werden ref()-funktionen auf die hr/stg dbt modelle
    - die entity-views müssen ergänzt werden um ref()-funktionen auf die hub/link/satellit modelle

    """

    # Read csv BDV_jobs to now the SR's
    # According DVB Docu --> Partial technical ID of the job : [Source System ID] + "_J_" + [Job Suffix ID]
    sr = []
    bdv_file = "./models/q1_datavault/dvb/BDV_jobs.csv"
    if os.path.exists(bdv_file):
        with open("./models/q1_datavault/dvb/BDV_jobs.csv", "r") as csvfile:
            csvreader = csv.reader(csvfile)
            sr = [row[0].upper().split("_J_", 1)[0] for row in csvreader]
            print("./models/q1_datavault/dvb/BDV_jobs.csv", " is exist")
    else:
        print("./BDV_jobs.csv", " is not exist")
    # print(sr)

    dv = parse_dvb_json("./models/q1_datavault/dvb")

    # Create directories
    dirNames = [
        "./models/q1_datavault/dbt/hubs",
        "./models/q1_datavault/dbt/links",
        "./models/q1_datavault/dbt/transaction_links",
        "./models/q1_datavault/dbt/satellites",
        "./models/q1_datavault/dbt/businessobjects",
        "./models/q1_datavault/dbt/staging",        
        "./models/q1_datavault/dbt/businessrules",
        "./models/q1_datavault/dbt/acesslayer"     
    ]

    if BR_PROCESIING == "RE" :
        print("------  RE  -----")
    else :
        print("------  SQLGLOT Tools  -----")    

    for dirName in dirNames:
        try:
            # Create target Directory
            os.makedirs(dirName)
            print("Directory ", dirName, " Created ")
        except FileExistsError:
            LOG.info("!!! Directory ", dirName, " already exists")
            print("Directory ", dirName, " already exists")

    LOG.info("Starting templates:")
    env = Environment(
        loader=FileSystemLoader(TEMPLATE_PATH_JINJA),
        autoescape=select_autoescape(["sql", "yml"]),
    )

    # Create Staging SQL
    if CREATE_STAGING :
        for stage in dv.stages.values(): 
            sname = stage.source_table_staging_id.lower()
            
            filePath = dirNames[5] + "/" + sname + ".sql"
            if os.path.exists(filePath):
                os.remove(filePath)
            for k, v in os.environ.items():
                env.globals[k] = v
                template = env.get_template("jinja_staging.sql").render(
                    sname=sname
                )
            #print(sname)
            with open(filePath, mode="w") as f:
                f.write(template)

    # Create hubs SQL
    for hub in dv.hubs.values():
        if not hub.loads:
            continue
        if hub.hub_id_of_alias_parent is None :
            filePath = dirNames[0] + "/" + hub.hub_id + ".sql"
            if os.path.exists(filePath):
                os.remove(filePath)
            filePath_doc = dirNames[0] + "/" + hub.hub_id + ".yml"
            if os.path.exists(filePath_doc):
                os.remove(filePath_doc)

            cbc = CBC(hub.hub_id)
            hname = hub.hub_id.lower()

            bks = []
            bk_load = []
            
            # keys_are_unique for all loads are False
            all_unique = False
            for hubload in hub.loads:
                all_unique = all_unique or hubload.keys_are_unique
            for hubload in hub.loads:
                # exclusion SR von hubs and Hub Load unique
                #if not sr.__contains__(hubload.system_id.upper()) and not ( not hubload.keys_are_unique and HUBLOADUNIQUE):
                if not sr.__contains__(hubload.system_id.upper()) and not ( not hubload.keys_are_unique and ( HUBLOADUNIQUE and all_unique )):
                    attr = []
                    col_name = []
                    lname = hubload.staging_table_id
                    staging_table = lname[
                        lname.find("_R_") + 3 : len(lname) 
                    ].lower()

                    for column in hubload.column_ids:
                        # add only column name
                        # print(column.column_id.split('.')[2])
                        col_name.append(column.column_id.split(".")[2])
                        attr.append(
                            Attribute(column.column_id.split(".")[2], None, None)
                        )

                    bks.append(
                        BusinessKey(
                            hname.split("_", 1)[1],
                            staging_table,
                            attr,
                            hubload.keys_are_unique,
                        )
                    )

                    # define staging table

                    for stage in dv.stages.values():
                        if stage.staging_table_id == lname:
                            bk_col_name = []
                            bk_col_type = []
                            for col in stage.columns:
                                if col.column_name in col_name:
                                    bk_col_name.append(col.column_name)
                                    bk_col_type.append(col.complete_data_type)
                            bk_load.append(
                                DocsColLoad(
                                    stage.staging_table_id, bk_col_name, bk_col_type
                                )
                            )
            cbc.keyset = bks

            fdocs = Docs(
                hub.hub_id,
                hub.hub_comment,
                [hname.split("_", 1)[1] + "_h", hname.split("_", 1)[1] + "_bk"],
                [HASHKEY_DATATYPE, BK_DATATYPE],
                [HASHKEY_COMMENT, BK_COMMENT],
            )
            fdocs.bk_colunm_load = bk_load
             #print(fdocs.bk_colunm_load)
            for k, v in os.environ.items():
                env.globals[k] = v
            template = env.get_template("jinja_hub.sql").render(cbc=cbc)
            # print(template)
            with open(filePath, mode="w") as f:
                f.write(template)

            # docs yml file
            template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
            with open(filePath_doc, mode="w") as f:
                f.write(template_docs)

    # Create hubs Alias SQL
    for hub in dv.hubs.values():
        if hub.hub_id_of_alias_parent is not None:
            filePath = dirNames[0] + "/" + hub.hub_id + ".sql"
            if os.path.exists(filePath):
                os.remove(filePath)

            filePath_doc = dirNames[0] + "/" + hub.hub_id + ".yml"
            if os.path.exists(filePath_doc):
                os.remove(filePath_doc)

            hname = hub.hub_id.split("_", 1)[1]
            halias = hub.hub_id_of_alias_parent.split("_", 1)[1]

            fdocs = Docs(
                hub.hub_id,
                hub.hub_comment + " Alias of " + halias,
                [hname.lower() + "_h", hname.lower() + "_bk"],
                [HASHKEY_DATATYPE, BK_DATATYPE],
                [HASHKEY_COMMENT, BK_COMMENT],
            )

            for k, v in os.environ.items():
                env.globals[k] = v
            template = env.get_template("jinja_hub_alias.sql").render(
                hname=hname, halias=halias
            )
            # print(template)
            with open(filePath, mode="w") as f:
                f.write(template)

            # docs yml file
            template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
            with open(filePath_doc, mode="w") as f:
                f.write(template_docs)

    # Create links SQL
    for link in dv.links.values():
        if not link.loads:
            continue
        filePath = dirNames[1] + "/" + link.link_id + ".sql"
        if os.path.exists(filePath):
            os.remove(filePath)

        filePath_doc = dirNames[1] + "/" + link.link_id + ".yml"
        if os.path.exists(filePath_doc):
            os.remove(filePath_doc)

        fdocs = Docs(
            link.link_id,
            link.link_comment,
            [
                link.link_id.lower()[2:] + "_h",
                link.hub_a_id.lower()[2:] + "_h",
                link.hub_b_id.lower()[2:] + "_h",
            ],
            [HASHKEY_DATATYPE, HASHKEY_DATATYPE, HASHKEY_DATATYPE],
            [HASHKEY_COMMENT, HASHKEY_COMMENT, HASHKEY_COMMENT],
        )
        cbc_list = [link.hub_a_id, link.hub_b_id]

        nbrl = []
        for linkload in link.loads:
            attr = []
            hub_fields = [cbc_hub.split("_", 1)[1] for cbc_hub in cbc_list]
            # name of staging
            lname = linkload.staging_table_id
            staging_table = lname[lname.find("_R_") + 3 : len(lname)].lower()
            lbk = linkload.link_load_display_name.split(" > ")[1]
            lbk = lbk[lbk.find("(") + 1 : lbk.rfind(")")]
            rbk = linkload.link_load_display_name.split(" > ")[2]
            rbk = rbk[rbk.find("(") + 1 : rbk.rfind(")")]
            # add hks
            attr.append(Attribute(lbk + ", " + rbk, linkload.link_id[2:].lower(), None))
            attr.append(Attribute(lbk, hub_fields[0].lower(), None))
            attr.append(Attribute(rbk, hub_fields[1].lower(), None))

            cbc_rel = BusinessKey(None, None, attr, None)
            nbrl.append(NBRLoad(linkload.link_id[2:].lower(), staging_table, cbc_rel))

        nbr = NBR(link.link_id, link.link_type, nbrl)
        for k, v in os.environ.items():
            env.globals[k] = v
        template = env.get_template("jinja_link.sql").render(nbr=nbr)
        with open(filePath, mode="w") as f:
            f.write(template)
        # docs yml file
        template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
        with open(filePath_doc, mode="w") as f:
            f.write(template_docs)

    # Create transaction links SQL
    for tlink in dv.transaction_links.values():
        filePath = dirNames[2] + "/" + tlink.transaction_link_id + ".sql"
        if os.path.exists(filePath):
            os.remove(filePath)

        filePath_doc = dirNames[2] + "/" + tlink.transaction_link_id + ".yml"
        if os.path.exists(filePath_doc):
            os.remove(filePath_doc)
        
        lname = tlink.staging_table_id
        staging_table = lname[lname.find("_R_") + 3 : len(lname)].lower()
        cbc_list = []
        hrk = []
        doc_col_name = ['tlink_h']
        doc_col_type = [HASHKEY_DATATYPE]
        doc_col_comment = [HASHKEY_COMMENT]
        for hub in tlink.transaction_link_relations:
            doc_col_name.append(hub.linked_hub_id.split("_", 1)[1].lower() + '_h')
            doc_col_type.append(HASHKEY_DATATYPE)
            doc_col_comment.append(HASHKEY_COMMENT)
            cbc_list.append(hub.linked_hub_id.split("_", 1)[1].lower())
            for h in dv.hubs.values():
                if h.hub_id == hub.linked_hub_id:
                    hk = ''#[]
                    for hubload in h.loads:
                        if hubload.staging_table_id == lname:
                            hk = hubload.business_key_short.split(", ")
            hrk.append(hk)
        #print(cbc_list)
        #print(hrk)
        #print(staging_table)
        fdocs = Docs(tlink.transaction_link_id, tlink.transaction_link_comment, doc_col_name, doc_col_type, doc_col_comment)
        tnbr = TNBR(lname, staging_table, cbc_list, hrk)
        
        for k, v in os.environ.items():
            env.globals[k] = v
            template = env.get_template("jinja_transaction_link.sql").render(tnbr=tnbr)
            with open(filePath, mode="w") as f:
                f.write(template)        
        # docs yml file
        template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
        with open(filePath_doc, mode="w") as f:
            f.write(template_docs)
    
    # Create sats SQL
    for sat in dv.satellites.values():
        filePath = dirNames[3] + "/" + sat.satellite_id + ".sql"
        if os.path.exists(filePath):
            os.remove(filePath)

        filePath_doc = dirNames[3] + "/" + sat.satellite_id + ".yml"
        if os.path.exists(filePath_doc):
            os.remove(filePath_doc)

        cbc = CBC(sat.satellite_id)
        hub = sat.hub_id
        fdocs = Docs(sat.satellite_id, sat.satellite_comment, [], [], [])
        attr = []
        for h in dv.hubs.values():
            if h.hub_id == hub:
                for hubload in h.loads:
                    shk = []
                    if hubload.keys_are_unique:
                        for column in hubload.column_ids:
                            shk.append(column.column_id.split(".", 2)[2])
                        hk = ", ".join(shk)
        # add hk
        hname = sat.hub_id.split("_", 1)[1].lower()
        attr.append(Attribute(hk, hname + "_h", None))
        # add column_type and column_comment
        fdocs.column_name.append(hname + "_h")
        fdocs.column_type.append(HASHKEY_DATATYPE)
        fdocs.column_comment.append(HASHKEY_COMMENT)
        fdocs.column_name.append("hkdiff")
        fdocs.column_type.append(HASHKEY_DATATYPE)
        fdocs.column_comment.append(HASHKEY_COMMENT)

        for column in sat.column_ids:
            if "_offset_sec" in column.column_name:
                sat.column_ids.remove(column)
        # add hkdiff
        sbk = []
        for column in sat.column_ids:
            fdocs.column_name.append(column.column_name.replace(" ", "_"))
            if column.column_name in ["TIMESTAMP"]:
                column.column_name = "[" + column.column_name + "]"
            if column.column_name not in hk:
                sbk.append(column.column_name)
        dbk = ", ".join(sbk)
        attr.append(Attribute(dbk, "hkdiff", None))
        con = []
        lname = sat.staging_table_id
        staging_table = lname[lname.find("_R_") + 3 : len(lname)].lower()
        # add rest column
        for column in sat.column_ids:
            attr.append(Attribute(column.column_name, None, None))
            # fdocs.column_name.append( column.column_name)
            fdocs.column_type.append(column.complete_data_type)
            fdocs.column_comment.append(column.column_comment)
        con.append(Context(sat.satellite_id, staging_table, None, attr))
        cbc.context = con

        for k, v in os.environ.items():
            env.globals[k] = v
        template = env.get_template("jinja_sat.sql").render(cbc=cbc)

        with open(filePath, mode="w") as f:
            f.write(template)
        # docs yml file
        template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
        with open(filePath_doc, mode="w") as f:
            f.write(template_docs)

    # Create bos SQL
    for dm in dv.dimensional_model.values():
        #boname = dm.businessobject_dropdown_entry_string.replace(" ", "_") 
        boname = dm.businessobject_view_id.replace("BUSINESSOBJECTS.", "BUSINESSOBJECTS_")
        filePath = dirNames[4] + "/" + boname + ".sql"
        if os.path.exists(filePath):
            os.remove(filePath)

        filePath_doc = dirNames[4] + "/" + boname + ".yml"
        if os.path.exists(filePath_doc):
            os.remove(filePath_doc)

        #print("------")
        #print(dm.businessobject_set_id)
        #print(dm.businessobject_display_name)
        fdocs = Docs(boname, dm.businessobject_comment, [], [], [])
        count = -1
        path_list = []
        sources = []
        joins = []
        
        for bo in dm.businessobject_elements:
            count += 1
            body = json.loads(bo.path_id)["businessobject_path"]
            bo_str = bo_path(body, "")
            queries = bo_str.split(" --> ")
            path = queries[1:]
            query = queries[-1]
            query_name = "query_" + (str(count) if count > 10 else "0" + str(count))
            field = []
            for col in bo.columns:
                if "_offset_sec" not in col.column_name and "TRK" not in col.column_id and "SCD2" not in col.column_name and not col.column_name.endswith("LT") and not col.column_name.endswith("BKLT") and not col.column_name.endswith("LT_END"):
                    col_id = col.column_id.split(".")[2]
                    field.append(
                        Attribute(query_name + "." + col_id, col.column_name, None)
                    )
                    #ncol = col.column_name
                    #if ncol.find(" ") > 0:
                    #    ncol = "[" + ncol + "]"
                    #fdocs.column_name.append(ncol)    
                    fdocs.column_name.append(col.column_name.replace(" ", "_"))
                    
                    if col.column_name[0:3] == "BK_":
                        col.complete_data_type = BK_DATATYPE
                        if col.column_comment == "":
                            col.column_comment = BK_COMMENT
                    if col.column_name[0:3] == "HK_":
                        col.complete_data_type = HASHKEY_DATATYPE
                        col.column_comment = HASHKEY_COMMENT
                    fdocs.column_type.append(col.complete_data_type)
                    fdocs.column_comment.append(col.column_comment)

            sources.append(BusinessKey(query_name, query, field, None))
            #print(bo_str)
            for item in path:
                if "L_alias_default" in item:
                    path.remove(item)
            list_join = []
            voralias = ""
            vorrhf = ""
            vortype = ""
            for item in path:
                join = []
                if len(path) == 1:
                    join = ["  ", "", query_name, ""]
                    list_join.append(join)
                    break
                else:
                    type_join = "  left join "
                    if (
                        path.index(item) == len(path) - 1
                        and dm.granularity_satellite_id == path[-1]
                    ):
                        type_join = "  join "
                    tab = ""
                    item_name = path[path.index(item)]
                    item_vorname = path[path.index(item) - 1]
                    item_type = item_name[0:2]
                    if item_type == "L_":
                        lhf = item_name.split("_T_")[0][2:] + "_H"
                    elif item_type == "LT":
                        lhf = item_name.split("_F_")[1] + "_H" 
                    elif item_type == "S_":
                        lhf = (
                            item_name[item_name.find("S_") + 2 : item_name.rfind("_S_")]
                            + "_H"
                        )
                    else:
                        lhf = item_name[2:] + "_H"
                    if path.index(item) == 0:
                        al = "query_00"
                    elif path.index(item) == len(path) - 1:
                        al = query_name
                    else:
                        tab = item
                        al = (
                            query_name
                            + "_"
                            + (
                                str(path.index(item))
                                if path.index(item) > 10
                                else "0" + str(path.index(item))
                            )
                        )
                    rhf = lhf
                    if vortype == item_type:
                        rhf = vorrhf
                    rhf = voralias + "." + rhf
                    vorrhf = lhf
                    vortype = item_type
                    lhf = al + "." + lhf
                    voralias = al
                    join = [type_join, tab, al, " on " + lhf + " = " + rhf]
                    if path.index(item) > 0:
                        list_join.append(join)
            joins.append(list_join)

        joins.insert(1, joins.pop(len(joins) - 1))
        # print(joins)
        for k, v in os.environ.items():
            env.globals[k] = v
        template = env.get_template("jinja_bo.sql").render(sources=sources, joins=joins)
        # print(template)
        with open(filePath, mode="w") as f:
            f.write(template)
        # docs yml file
        template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
        with open(filePath_doc, mode="w") as f:
            f.write(template_docs)

    #Create BusinessRules SQL
   
    # list of accesslayer
    al_list =  []
    for br in dv.business_rules.values(): 
        if br.include_in_accesslayer :
            al_list.append(br.business_ruleset_view_id.split(".")[1])

    for br in dv.business_rules.values(): 
        brname = br.business_ruleset_view_id.replace("BUSINESS_RULES.", "BUSINESS_RULES_")
        dn = dirNames[6] 
        if br.include_in_accesslayer :
            dn = dirNames[7] 
            brname = brname.replace("BUSINESS_RULES", "ACCESSLAYER")
        brview = br.business_rules_view_code.upper()

        fdocs = Docs(brname, br.business_ruleset_comment, [], [], [])
        for col in br.column_nq_ids:
            fdocs.column_name.append(col.replace(" ", "_"))
            fdocs.column_type.append("UNDEFINED")
                
        index = brview.find("CREATE")
        lindex = [] 
        if index >= 0:
            lindex.append(brview.find("SELECT"))
            lindex.append(brview.find("WITH"))
            ip = min([i for i in lindex if i > 0])
            brview = brview[ip:]
        comma = ""
        index = brview.find("DERIVED FROM")
        if index >= 0:    
            brview = "SELECT \n  " 
            for index, col in enumerate(br.column_nq_ids):
                if col.find(" ") > 0:
                    col = '"' + col + '"' 
                comma = "" if index == len(br.column_nq_ids) -1 else ","    
                brview = brview + col + comma + "\n  "
            brview = brview + "\nFROM " + br.dependent_objects[0].object_id

        if BR_PROCESIING == "RE" :
        # use RE
            brview = re.sub(r"(DATAVAULT\.S_)([\w-]+)(_C)", r"{{ ref('S_\2') }}", brview)
            brview = re.sub(r'STAGING\."([\w-]+)"', r"{{ ref('\1') }}", brview)
            brview = re.sub(r'DATAVAULT\.([\w-]+)', r"{{ ref('\1') }}", brview)
            brview = re.sub(r'(BUSINESSOBJECTS|PUBLIC)\.([\w-]+)', r"{{ ref('\1.\2') }}", brview)
        
        else: 
        # SQLGLOT Tools
            table_list = []
            for table in parse_one(brview, read=SQLGLOT_DIALECT, error_level=ErrorLevel.IGNORE).find_all(exp.Table):
                table_list.append(table.db + "." + table.name)
            table_list =list(set(table_list))

            for item in table_list:
                if item.startswith('DATAVAULT.S_') and item.endswith('_C') : 
                    brview = brview.replace(item, "{{ ref('" + item[:-2].rsplit(".")[1] + "') }} ")
                if item.startswith('STAGING.') or item.startswith('DATAVAULT.'):  
                    brview = brview.replace(item, "{{ ref('" + item.rsplit(".")[1] + "') }} ")
                if item.startswith('BUSINESSOBJECTS.') or item.startswith('PUBLIC.'): 
                    brview = brview.replace(item, "{{ ref('" + item.replace("BUSINESSOBJECTS.", "BUSINESSOBJECTS_").replace("PUBLIC.","PUBLIC_") + "') }} ")   
        
        br_list = re.findall(r'BUSINESS_RULES\.([\w-]+)', brview)
        for item in br_list:
            patt = "BUSINESS_RULES_"
            if item in al_list:
                patt = "ACCESSLAYER_"
            brview = brview.replace("BUSINESS_RULES." + item + " ", "{{ ref('" + patt + item + "') }} ")


        filePath = dn + "/" + brname + ".sql"
        if os.path.exists(filePath):
             os.remove(filePath)
        for k, v in os.environ.items():
            env.globals[k] = v
            template = env.get_template("jinja_br.sql").render(
                brname=brname, brview=brview
            )
        with open(filePath, mode="w") as f:
            f.write(template)

        # docs yml file
        filePath_doc = dn + "/" + brname + ".yml"
        if os.path.exists(filePath_doc):
            os.remove(filePath_doc)
        
        template_docs = env.get_template("docs.yml").render(fdocs=fdocs)
        with open(filePath_doc, mode="w") as f:
            f.write(template_docs)    


if __name__ == "__main__":
    sys.exit(main())
