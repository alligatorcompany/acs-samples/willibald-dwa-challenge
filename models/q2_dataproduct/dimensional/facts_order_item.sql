{% set sql_query = run_query('select numberofdaysfrom, numberofdaysuntil, range_indicator from '
~ ref('dim_on_time_delivery')) %}

with
    order_data as (
        select *
        from {{ ref('dim_order') }}
    ),

    product as (
        select *
        from {{ ref('dim_product') }}
    ),

    delivery_address as (
        select *
        from {{ ref('dim_delivery_address') }}
    ),

    sales_date as (
        select *
        from {{ ref('dim_sales_date') }}
    ),

    on_time_delivery as (
        select *
        from {{ ref('dim_on_time_delivery') }}
    ),

    raw_data as (
        select
            order_data.order_type as order_type,
            order_data.bestellungid as bp_bestellungid,
            order_data.posid as bp_posid,
            delivery_address.bestellungid as l_bestellungid,
            delivery_address.posid as l_posid,
            order_data.rabatt,
            order_data.lieferdatum as deliverydate,
            order_data.wunschdatum as requestdate,
            order_data.menge,
            order_data.preis
        from order_data
        -- Capture Products only found in Deliveries
        left outer join product
            on order_data.produktid = product.produktid
        left outer join delivery_address
            on
                order_data.bestellungid = delivery_address.bestellungid
                and order_data.posid = delivery_address.posid
        --join sales_date
        inner join sales_date
            on order_data.kaufdatum = sales_date.sales_date
    ),

    data_prep as (
        select
            order_type,
            bp_bestellungid,
            bp_posid,
            l_bestellungid,
            l_posid,
            requestdate,
            deliverydate,
            menge as quantity,
            row_number() over (partition by bp_bestellungid order by bp_bestellungid) as reihenfilter,
            (menge * preis) * ((100 - rabatt) / 100) as revenue,
            --wird jetzt als 0 hinterlegt, weil kein vertriebspartner zum Kunden gehört
            case when bp_bestellungid = l_bestellungid and bp_posid = l_posid then 1
            --rs item_delivered = 1, da keine Lieferung erfolgt
            when order_type = 'rs' then 1
            else 0 end as is_item_delivered,
            case
                --when deliverydate is null then 0
                when deliverydate is null and order_type = 'ws' then cast('yyy' as char(3))
                -- zzz keine Lieferung, deshalb zzz für Sale (irrelevant für Termintreue)
                when order_type = 'rs'
                    then cast('zzz' as char(3))
                else
                {% if target.name == 'dev' %}
                    days_between(to_date(deliverydate, 'dd.mm.yyyy'), to_date(requestdate, 'dd.mm.yyyy'))
                {% elif target.name == 'dev_snow' %}
                cast(
                    datediff(day, to_date(requestdate, 'dd.mm.yyyy'), to_date(deliverydate, 'dd.mm.yyyy')) as varchar(
                        20
                    )
                )
                {% else %}
                    days_between(to_date(deliverydate, 'dd.mm.yyyy'), to_date(requestdate, 'dd.mm.yyyy'))
                {% endif %}
            end as datediff
        from raw_data
    ),

    data_prep_2 as (
        select
            order_type,
            bp_bestellungid,
            bp_posid,
            l_bestellungid,
            l_posid,
            quantity,
            revenue,
            is_item_delivered,
            cast(
                case
                    when datediff = 'yyy' then 1500000
                    when datediff = 'zzz' then 2000000
                    else datediff
                end as int
            ) as order_deviation,
            min(reihenfilter) as reihenfilter
        from data_prep
        group by
            order_type,
            bp_bestellungid,
            bp_posid,
            l_bestellungid,
            l_posid,
            reihenfilter,
            quantity,
            revenue,
            is_item_delivered,
            datediff
    ),

    resultset as (
        select
            order_type,
            bp_bestellungid,
            bp_posid,
            quantity,
            revenue,
            is_item_delivered,
            order_deviation,
            reihenfilter,
            case
                when reihenfilter = 1 and is_item_delivered != 1 then 1
                else 0
            end as open_order,
            case
                {%- for row in sql_query %}
                {% if row[0] != 'zzz' and row[0] != 'yyy' and row[0] != 'xxx' %}
                when
                    order_type = 'ws' and order_deviation
                    between cast('{{ row[0] }}' as int) and cast('{{ row[1] }}' as int)
                    then cast('{{ row[2] }}' as varchar(20))
                {% endif %}
                {%- endfor %}
                when order_deviation = 1500000 then 'yyy'
                when order_deviation = 2000000 then 'zzz'
            end as range_indicator
        from data_prep_2
        group by
            order_type,
            bp_bestellungid,
            bp_posid,
            reihenfilter,
            quantity,
            revenue,
            is_item_delivered,
            order_deviation
    ),

    final_data as (
        select
            order_type,
            bp_bestellungid as order_id,
            bp_posid as order_item_id,
            quantity,
            revenue,
            is_item_delivered,
            open_order,
            range_indicator,
            case
                when reihenfilter = 1 and open_order = 0 then 1
                else 0
            end as order_complete,
            case
                when order_deviation = 1500000 then cast('yyy' as varchar(20))
                when order_deviation = 2000000 then cast('zzz' as varchar(20))
                else cast(order_deviation as varchar(20))
            end as order_deviation
        from resultset
        group by
            order_type,
            bp_bestellungid,
            bp_posid,
            quantity,
            revenue,
            is_item_delivered,
            open_order,
            reihenfilter,
            order_deviation,
            range_indicator
        order by
            order_id,
            order_item_id
    )

select
    final_data.order_type,
    final_data.order_id,
    final_data.order_item_id,
    final_data.quantity,
    final_data.revenue,
    final_data.is_item_delivered,
    final_data.open_order,
    final_data.order_complete,
    final_data.order_deviation,
    final_data.range_indicator
from final_data
left join on_time_delivery
    on final_data.range_indicator = on_time_delivery.range_indicator
group by
    final_data.order_type,
    final_data.order_id,
    final_data.order_item_id,
    final_data.quantity,
    final_data.revenue,
    final_data.is_item_delivered,
    final_data.open_order,
    final_data.order_complete,
    final_data.order_deviation,
    final_data.range_indicator
order by
    final_data.order_id,
    final_data.order_item_id
