with
    sale as (

        select * from
        {% if target.name == 'docs' %}
{{ ref('BUSINESSOBJECTS_SALE_S_ROAD') }}
{% else %}
            {{ source('businessobjects','sale_s_road') }}
        {% endif %}

    ),

    item as (

        select * from
        {% if target.name == 'docs' %}
{{ ref('BUSINESSOBJECTS_SALE_ITEM_S_ROAD_C_SCD_H') }}
{% else %}
            {{ source('businessobjects','sale_item_s_road') }}
        {% endif %}

    ),

    rs_bestellung as (
        select
            sale.sale_h,
            sale.bestellungid,
            sale.kundeid,
            sale.vereinspartnerid,
            item.kaufdatum,
            sale.kreditkarte,
            sale.gueltigbis,
            sale.kkfirma,
            item.produktid,
            item.menge,
            item.preis,
            item.rabatt
        from sale
        inner join item
            on sale.bestellungid = item.bestellungid
        where
            sale.fv_sale = true
            and item.fv_sale_item = true
    )

select * from rs_bestellung
