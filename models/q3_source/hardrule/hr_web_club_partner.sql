{{ config (materialized='view') }}

select
    {{ dbt_utils.star(ref('stg_web_club_partner')) }}
from {{ ref('stg_web_club_partner') }}
