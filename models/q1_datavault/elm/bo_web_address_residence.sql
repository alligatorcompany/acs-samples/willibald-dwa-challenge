SELECT
    {{ dbt_utils.star(from=source('businessobjects','address_of_residence_s_web')) }}
{% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_ADDRESS_OF_RESIDENCE_S_DVB_HUB') }}
{% else %}
FROM {{ source('businessobjects','address_of_residence_s_web') }}
{% endif %}
WHERE FV_ADDRESS_OF_RESIDENCE = true
