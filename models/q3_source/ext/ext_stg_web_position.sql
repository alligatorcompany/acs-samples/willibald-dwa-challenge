{{ config (materialized='view') }}
{% set column_desc_query %}
    select 
        case 
            when column_name in ('PosID', 'ProduktID', 'SpezLieferAdrID', 'Menge') then 'cast(replace(cast(parse_json(value)' || ':c' || (order_id+1) || ' as varchar), '','' , ''.'')' || ' as decimal(18,0))' || ' as '|| column_name
            when column_name in ('Preis') then 'cast(replace(cast(parse_json(value)' || ':c' || (order_id+1) || ' as varchar), '','' , ''.'')' || ' as decimal(18,2))' || ' as '|| column_name
            else 'parse_json(value)' || ':c' || (order_id+1) || '::' || 'varchar' || ' as '|| column_name
        end
    from table(
        infer_schema(
            location => '@DEV_NIKLAS.DWH_01_EXT.DDVUG_WILLIBALD_SAMEN_S3_STAGE/ldts/webshop/position/'
            ,file_format => 'DEV_NIKLAS.DWH_01_EXT.CSV_SCHEMA_DETECTION')
        )
{% endset %}

{% set results = run_query(column_desc_query) %}

{% if execute %}
{# Return the first column #}
{% set results_list = results.columns[0].values() %}
{% else %}
{% set results_list = [] %}
{% endif %}

select
{% for column_desc in results_list %}
{{ column_desc }} {% if not loop.last %},{% endif %}
{% endfor %}
from {{ source('ext','ext_webshop_position') }}
