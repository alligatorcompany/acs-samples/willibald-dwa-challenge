select
    pc.katid,
    pc.oberkatid,
    pc.name,
    cp.periode
    {% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_PRODUCT_CATEGORY_S_DVB_HUB') }}
{% else %}
from
    {{ source('businessobjects','product_category_s_web_c_scd2_h') }}
    {% endif %}
        as pc
inner join {{ ref('bo_web_product_category_period_calc') }} as cp
    on pc.flvc_product_category = cp.flvc_product_category
where
    pc.flvc_product_category is not null
    and pc.fv_product_category = true
    and pc.katid is not null
