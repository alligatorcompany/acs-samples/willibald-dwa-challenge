{{ config (materialized='view') }}

select
    {{ dbt_utils.star(ref('stg_web_order')) }}
from {{ ref('stg_web_order') }}
