--Erst mit der dritten und letzten Periode relevant
with
    src as (
        select kundeid
        from {{ source('webshop_periode_1','kunde_periode_1') }}
        where
            kundeid not in
            (
                select kundeid
                from {{ source('webshop_periode_2','kunde_periode_2') }}
            )
            and kundeid in
            (
                select kundeid
                from {{ source('webshop_periode_3','kunde_periode_3') }}
            )
    ),

    dv as (
        select kundeid
        from {{ ref('bo_web_customer_period') }}
        where fv_customer = false
    ),

    src_minus_dv as (

        select kundeid from src
        except
        select kundeid from dv

    ),

    dv_minus_src as (

        select kundeid from dv
        except
        select kundeid from src

    ),

    unioned as (

        select
            'src_minus_dv' as which_diff,
            src_minus_dv.*
        from src_minus_dv
        union all
        select
            'dv_minus_src' as which_diff,
            dv_minus_src.*
        from dv_minus_src

    )

select * from unioned
