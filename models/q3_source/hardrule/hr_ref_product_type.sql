{{ config (materialized='view') }}

select
    {{ dbt_utils.star(ref('stg_ref_product_type')) }}
from {{ ref('stg_ref_product_type') }}
