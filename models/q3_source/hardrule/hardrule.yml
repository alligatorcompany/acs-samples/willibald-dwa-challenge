version: 2

models:
  - name: hr_href_on_time_delivery
    description: |
      Hardrule View containing data to determine whether a delivery was arriving to early, on time or to late
    columns:
      - name: '"Anzahl Tage von"'
        data_type: varchar
        description: |
          Number of days from which a delivery is within a defined status of an on time delivery
        tests:
          - unique
          - not_null
      - name: '"Anzahl Tage bis"'
        data_type: varchar
        description: |
          Number of days until which a delivery is within a defined status of an on time delivery
        tests:
        #  - unique
          - not_null
      - name: bezeichnung
        data_type: varchar
        description: |
          Description for classifying deliveries
      - name: bewertung
        data_type: varchar
        description: |
          Description for ranking the performance of the delivery

  - name: hr_ref_product_type
    description: |
      Hardrule View containing data of all available product typs
    columns:
      - name: typ
        data_type: varchar
        description: |
          Unique identifier for the type of product
        tests:
          - unique
          - not_null
      - name: bezeichnung
        data_type: varchar
        description: |
          Full Name of the product

  - name: hr_web_address_residence
    description: |
      Hardrule View containing data of the address of residence for each customer
    tests:
      - unique:
          column_name: "(kundeid || '-' || von)"
    columns:
      - name: kundeid
        data_type: number
        description: |
          Unique identifier of a customer
        tests:
          - not_null
      - name: von
        data_type: varchar
        description: |
          Start date address
      - name: bis
        data_type: varchar
        description: |
          End date address
      - name: strasse
        data_type: varchar
        description: |
          Street Name
      - name: hausnummer
        data_type: varchar
        description: |
          House number
      - name: plz
        data_type: number
        description: |
          Zip Code of residence
      - name: ort
        data_type: varchar
        description: |
          City of residence
      - name: land
        data_type: number
        description: |
          Country of residence
      - name: adresszusatz
        data_type: varchar
        description: |
          Address supplement

  - name: hr_web_club_partner
    description: |
      Hardrule View containing data of all club partners
    columns:
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
        tests:
          - unique
          - not_null
      - name: kundeidverein
        data_type: number
        description: |
          Unique identifier of customer representing a club partner
      - name: rabatt1
        data_type: number
        description: |
          Discount 1
      - name: rabatt2
        data_type: number
        description: |
          Discount 2
      - name: rabatt3
        data_type: number
        description: |
          Discount 3

  - name: hr_web_customer
    description: |
      Hardrule View containing data for all customers
    columns:
      - name: kundeid
        data_type: varchar
        description: |
          Unique identifier of a customer
        tests:
          - unique
          - not_null
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
      - name: vorname
        data_type: varchar
        description: |
          First Name of customer
      - name: name
        data_type: varchar
        description: |
          Last Name of customer
      - name: geschlecht
        data_type: varchar
        description: |
          Gender of customer
      - name: geburtsdatum
        data_type: varchar
        description: |
          Date of birth
      - name: telefon
        data_type: varchar
        description: |
          Telefon number
      - name: mobil
        data_type: varchar
        description: |
          Mobil telefon number
      - name: email
        data_type: varchar
        description: |
          Email Address
      - name: kreditkarte
        data_type: varchar
        description: |
          Credit card number
      - name: gueltigbis
        data_type: varchar
        description: |
          Month and Year to which the Credit card is valid
      - name: kkfirma
        data_type: varchar
        description: |
          Name of Credit card company who issued the credit card

  - name: hr_web_delivery_address
    description: |
      Hardrule View containing data for all available delivery addresses of each customers
    columns:
      - name: lieferadrid
        data_type: varchar
        description: |
          Unique identifier of a delivery address
        tests:
          - unique
          - not_null
      - name: kundeid
        data_type: varchar
        description: |
          Unique identifier of a customer
      - name: strasse
        data_type: varchar
        description: |
          Street Name
      - name: hausnummer
        data_type: varchar
        description: |
          House number
      - name: adresszusatz
        data_type: varchar
        description: |
          Address supplement
      - name: plz
        data_type: varchar
        description: |
          Zip Code of residence
      - name: ort
        data_type: varchar
        description: |
          City of residence
      - name: land
        data_type: number
        description: |
          Country of residence

  - name: hr_web_delivery_service
    description: |
      Hardrule View containing data for all available delivery services
    columns:
      - name: lieferdienstid
        data_type: varchar
        description: |
          Unique identifier of a delivery service
        tests:
          - unique
          - not_null
      - name: name
        data_type: varchar
        description: |
          Name of delivery service
      - name: telefon
        data_type: varchar
        description: |
          Telefon number
      - name: fax
        data_type: varchar
        description: |
          Fax number
      - name: email
        data_type: varchar
        description: |
          Email Address
      - name: strasse
        data_type: varchar
        description: |
          Street Name
      - name: hausnummer
        data_type: varchar
        description: |
          House number
      - name: plz
        data_type: varchar
        description: |
          Zip Code of residence
      - name: ort
        data_type: varchar
        description: |
          City of residence
      - name: land
        data_type: number
        description: |
          Country of residence

  - name: hr_web_delivery
    description: |
      Hardrule View containing data for each delivery of an order
    tests:
      - unique:
          column_name: "(bestellungid || '-' || posid)"
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
        tests:
          - not_null
      - name: posid
        data_type: number
        description: |
          Unique identifier of the order line item
        tests:
          - not_null
      - name: lieferadrid
        data_type: number
        description: |
          Unique identifier of a delivery address
        tests:
          - not_null
      - name: lieferdienstid
        data_type: number
        description: |
          Unique identifier of a delivery service
      - name: lieferdatum
        data_type: varchar
        description: |
          Date on which the product is scheduled to be delivered

  - name: hr_web_order
    description: |
      Hardrule View containing data for all placed orders in the webshop
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
        tests:
          - unique
          - not_null
      - name: kundeid
        data_type: number
        description: |
          Unique identifier of a customer
      - name: allglieferadrid
        data_type: number
        description: |
          Reference to LieferAdrID in delivery address
      - name: bestelldatum
        data_type: varchar
        description: |
          Date on which the order was placed
      - name: wunschdatum
        data_type: varchar
        description: |
          Date on which the customer whishes the order to be delivered
      - name: rabatt
        data_type: number
        description: |
          Discount on price in percentage

  - name: hr_web_position
    description: |
      Hardrule View containing data for each order line of an order
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
        tests:
          - not_null
      - name: posid
        data_type: number
        description: |
          Unique identifier of the order line item
        tests:
          - unique
          - not_null
      - name: produktid
        data_type: number
        description: |
          Unique identifier of a product
      - name: spezlieferadrid
        data_type: number
        description: |
          Reference to LieferAdrID in delivery address
      - name: menge
        data_type: number
        description: |
          Number of items ordered
      - name: preis
        data_type: number
        description: |
          Price of the product

  - name: hr_web_product_category
    description: |
      Hardrule View containing data of all available product categories
    columns:
      - name: katid
        data_type: varchar
        description: |
          Unique identifier of a product category
        tests:
          - unique
          - not_null
      - name: oberkatid
        data_type: varchar
        description: |
          Unique identifier of a top product category
      - name: name
        data_type: varchar
        description: |
          Full Name of product category

  - name: hr_web_product
    description: |
      Hardrule View containing data for each available product
    columns:
      - name: produktid
        data_type: number
        description: |
          Unique identifier of a product
        tests:
          - unique
          - not_null
      - name: katid
        data_type: varchar
        description: |
          Unique identifier of a product category
      - name: bezeichnung
        data_type: varchar
        description: |
          Full Name of the product
      - name: umfang
        data_type: varchar
        description: |
          Description of size or scale of the product
      - name: typ
        data_type: number
        description: |
          Unique identifier for the type of product
      - name: preis
        data_type: number
        description: |
          Price of the product
      - name: pflanzort
        data_type: varchar
        description: |
          Description of where Plants should be planted
      - name: pflanzabstand
        data_type: varchar
        description: |
          Description of how Plants should be planted

  - name: hr_rs_position_kk
    description: |
      Hardrule View containing data for each order line of an order at a roadshow from an unknown customer
    tests:
      - unique:
          column_name: "(bestellungid || '-' || produktid)"
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
        tests:
          - not_null
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
      - name: produktid
        data_type: number
        description: |
          Unique identifier of a product
      - name: kaufdatum
        data_type: varchar
        description: |
          Date on which the product was bought
      - name: menge
        data_type: number
        description: |
          Number of items ordered
      - name: preis
        data_type: number
        description: |
          Price of the product
      - name: rabatt
        data_type: number
        description: |
          Discount on price in percentage

  - name: hr_rs_position_mk
    description: |
      Hardrule View containing data for each order line of an order at a roadshow with a known customer
    tests:
      - unique:
          column_name: "(bestellungid || '-' || produktid)"
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
        tests:
          - not_null
      - name: kundeid
        data_type: number
        description: |
          Unique identifier of a customer
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
      - name: produktid
        data_type: number
        description: |
          Unique identifier of a product
      - name: kaufdatum
        data_type: varchar
        description: |
          Date on which the product was bought
      - name: menge
        data_type: number
        description: |
          Number of items ordered
      - name: preis
        data_type: number
        description: |
          Price of the product
      - name: rabatt
        data_type: number
        description: |
          Discount on price in percentage

  - name: hr_rs_order_mk
    description: |
      Hardrule View containing data for all placed orders during the roadshow where a customer is known
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
        tests:
          - unique
          - not_null
      - name: kundeid
        data_type: number
        description: |
          Unique identifier of a customer
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
      - name: kreditkarte
        data_type: varchar
        description: |
          Credit card number
      - name: gueltigbis
        data_type: varchar
        description: |
          Month and Year to which the Credit card is valid
      - name: kkfirma
        data_type: varchar
        description: |
          Name of Credit card company who issued the credit card

  - name: hr_rs_order_kk
    description: |
      Hardrule View containing data for all placed orders during the roadshow where a customer is known
    columns:
      - name: bestellungid
        data_type: varchar
        description: |
          Unique identifier of an order
        tests:
          - unique
          - not_null
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
      - name: kreditkarte
        data_type: varchar
        description: |
          Credit card number
      - name: gueltigbis
        data_type: varchar
        description: |
          Month and Year to which the Credit card is valid
      - name: kkfirma
        data_type: varchar
        description: |
          Name of Credit card company who issued the credit card

  - name: hr_rs_roadshow
    description: |
      Hardrule View linking club partner to roadshow as roadshows are held at club partners
    columns:
      - name: vereinspartnerid
        data_type: varchar
        description: |
          Full Name of a club partner
        tests:
          - not_null
      - name: kaufdatum
        data_type: varchar
        description: |
          Date on which the product was bought
        tests:
          - unique # mehrere Roadshow am selben Tag?
          - not_null
