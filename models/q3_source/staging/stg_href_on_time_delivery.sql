{% set source_name = 'href_termintreue_periode_' ~ var('periode_tag','1') %}
{% set table_name = 'href_termintreue_periode_' ~ var('periode_tag','1') %}

{{ config (materialized='view') }}

{% if var("external_tables") == false %}
-- Seeds
{% if var("periode_tag")=='1'%}
select
    {{ dbt_utils.star(source(source_name, table_name)) }}
from {{ source(source_name, table_name) }}
{% else %}
    select
      {{ dbt_utils.star(source('href_termintreue_periode_2', 'href_termintreue_periode_2')) }}
    from {{ source('href_termintreue_periode_2', 'href_termintreue_periode_2') }}
  {% endif %}
{% else %}
-- External Tables
  select
    {{ dbt_utils.star(source('q3_ext', 'ext_stg_href_on_time_delivery')) }}
  from {{ source('q3_ext', 'ext_stg_href_on_time_delivery') }}
{% endif %}
