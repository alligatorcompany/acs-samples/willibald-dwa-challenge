{{ config (materialized='view') }}

{% if var("external_tables") == false %}
-- Seeds
{% if var("periode_tag")=='1'%}
select
    {{ dbt_utils.star(source('webshop_periode_1', 'produktkategorie_periode_1')) }},
    1 as periode
from {{ source('webshop_periode_1', 'produktkategorie_periode_1') }}
{% elif var("periode_tag")=='2'%}
  select
    {{ dbt_utils.star(source('webshop_periode_1', 'produktkategorie_periode_1')) }},
    1 as periode
  from {{ source('webshop_periode_1', 'produktkategorie_periode_1') }}
  union
  select
    {{ dbt_utils.star(source('webshop_periode_2', 'produktkategorie_periode_2')) }},
    2 as periode
  from {{ source('webshop_periode_2', 'produktkategorie_periode_2') }}
  {% else %}
  select
    {{ dbt_utils.star(source('webshop_periode_1', 'produktkategorie_periode_1')) }},
    1 as periode
  from {{ source('webshop_periode_1', 'produktkategorie_periode_1') }}
  union
  select
    {{ dbt_utils.star(source('webshop_periode_2', 'produktkategorie_periode_2')) }},
    2 as periode
  from {{ source('webshop_periode_2', 'produktkategorie_periode_2') }}
  union
  select
    {{ dbt_utils.star(source('webshop_periode_3', 'produktkategorie_periode_3')) }},
    3 as periode
  from {{ source('webshop_periode_3', 'produktkategorie_periode_3') }}
  {% endif %}
{% else %}
-- External Tables
select
    {{ dbt_utils.star(source('q3_ext', 'ext_stg_web_order')) }}
    from {{ source('q3_ext', 'ext_stg_web_order') }}
{% endif %}
