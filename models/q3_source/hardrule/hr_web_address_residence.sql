{{ config (materialized='view') }}

select
    {{ dbt_utils.star(ref('stg_web_address_residence')) }}
from {{ ref('stg_web_address_residence') }}
