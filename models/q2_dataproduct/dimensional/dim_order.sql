with
    ws_order as (
        select *
        from {{ ref('bo_web_order') }}
    ),

    order_item as (
        select *
        from {{ ref('bo_web_position') }}
    ),

    customer as (
        select *
        from {{ ref('bo_web_customer') }}
    ),

    club_partner as (
        select *
        from {{ ref('bo_web_club_partner') }}
    ),

    delivery as (
        select *
        from {{ ref('bo_web_delivery') }}
    ),

    delivery_address as (
        select *
        from {{ ref('bo_web_delivery_address') }}
    ),

    rs_order as (
        select *
        from {{ ref('bo_road_rs_order') }}
    ),

    ws_order_data as (
        select
            ws_order.order_h,
            cast(ws_order.bestellungid as varchar(2000000)) as bestellungid,
            cast(ws_order.kundeid as varchar(2000000)) as kundeid,
            cast(ws_order.bestelldatum as varchar(2000000)) as bestelldatum,
            --als Eqivalent zu Kaufdatum in RS
            ws_order.bestelldatum as kaufdatum,
            cast(ws_order.wunschdatum as varchar(2000000)) as wunschdatum,
            ws_order.rabatt,
            cast(order_item.order_item_h as varchar(2000000)) as order_item_h,
            order_item.posid,
            order_item.produktid,
            order_item.menge,
            order_item.preis,
            customer.customer_h,
            customer.vereinspartnerid,
            cast(delivery.delivery_h as varchar(2000000)) as delivery_h,
            cast(delivery.lieferdatum as varchar(2000000)) as lieferdatum
        from ws_order
        inner join order_item
            on ws_order.bestellungid = order_item.bestellungid
        inner join customer
            on ws_order.kundeid = customer.kundeid
        -- Capture Orders which have no Club Partner
        left outer join club_partner
            on customer.vereinspartnerid = club_partner.vereinspartnerid
        -- Capture Orders which have no Deliveries
        left outer join delivery
            on
                order_item.bestellungid = delivery.bestellungid
                and order_item.posid = delivery.posid
        -- with inner join without 49 Rows without Delivery Address
        left outer join delivery_address
            on delivery.lieferadrid = delivery_address.lieferadrid
    ),

    rs_order_data as (
        select
            rs_order.sale_h as order_h,
            rs_order.bestellungid,
            cast(rs_order.kundeid as varchar(2000000)) as kundeid,
            -- Sale has no Delivery
            cast(null as varchar(2000000)) as bestelldatum,
            rs_order.kaufdatum,
            -- Sale has no Delivery
            cast(null as varchar(2000000)) as wunschdatum,
            rs_order.rabatt,
            cast(null as varchar(2000000)) as order_item_h,
            rs_order.produktid,
            rs_order.menge,
            rs_order.preis,
            customer.customer_h,
            customer.vereinspartnerid,
            cast(null as varchar(2000000)) as delivery_h,
            cast(null as varchar(2000000)) as lieferdatum,
            row_number() over (partition by rs_order.bestellungid order by rs_order.bestellungid) as posid
        from rs_order
        -- Capture Orders which have no Customer
        left outer join customer
            on rs_order.kundeid = customer.kundeid
        -- Capture Orders which have no Club_Partner
        left outer join club_partner
            on rs_order.vereinspartnerid = club_partner.vereinspartnerid
    --Keine delivery, deshalb kein join auf delivery
    )

select
    'ws' as order_type,
    order_h,
    bestellungid,
    kundeid,
    bestelldatum,
    kaufdatum,
    wunschdatum,
    rabatt,
    order_item_h,
    posid,
    produktid,
    menge,
    preis,
    customer_h,
    vereinspartnerid,
    delivery_h,
    lieferdatum
from ws_order_data
union all
select
    'rs' as order_type,
    order_h,
    bestellungid,
    kundeid,
    bestelldatum,
    kaufdatum,
    wunschdatum,
    rabatt,
    order_item_h,
    posid,
    produktid,
    menge,
    preis,
    customer_h,
    vereinspartnerid,
    delivery_h,
    lieferdatum
from rs_order_data
