{% set source_name = 'roadshow_tag_' ~ var('periode_tag','1') %}
{% set table_name = 'rs_bestellung_tag_' ~ var('periode_tag','1') %}

{{ config (materialized='view') }}

{% if var("external_tables") == false %}
-- Seeds
select
    {{ dbt_utils.star(source(source_name, table_name)) }}
from {{ source(source_name, table_name) }}
{% else %}
-- External Tables
  select
    {{ dbt_utils.star(source('q3_ext', 'ext_stg_rs_order')) }}
  from {{ source('q3_ext', 'ext_stg_rs_order') }}
{% endif %}
