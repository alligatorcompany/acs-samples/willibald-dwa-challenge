with
    dim_sales_date as (
        select
            cast(to_char(date_day, 'dd.mm.yyyy') as varchar(2000000)) as sales_date,
            month_name as sales_month, --noch auf zahl nicht name des monats
            year_actual as sales_year,
            cast(year_actual as char(4))
            || '-'
            || cast(week_of_year as char(2)) as sales_week
        from {{ ref('bo_date_calc') }}
        where
            date_day between to_date('03/01/2022', 'mm/dd/yyyy')
            and to_date('04/01/2022', 'mm/dd/yyyy')
    )

select *
from dim_sales_date
