{{ config (materialized='view') }}
{% set column_desc_query %}
    select 
        'parse_json(value)' || ':c' || (order_id+1) || '::' || 'varchar' || ' as '|| column_name
    from table(
        infer_schema(
            location => '@DEV_NIKLAS.DWH_01_EXT.DDVUG_WILLIBALD_SAMEN_S3_STAGE/ldts/webshop/lieferdienst/'
            ,file_format => 'DEV_NIKLAS.DWH_01_EXT.CSV_SCHEMA_DETECTION')
        )
{% endset %}

{% set results = run_query(column_desc_query) %}

{% if execute %}
{# Return the first column #}
{% set results_list = results.columns[0].values() %}
{% else %}
{% set results_list = [] %}
{% endif %}

select
{% for column_desc in results_list %}
{{ column_desc }} {% if not loop.last %},{% endif %}
{% endfor %}
from {{ source('ext','ext_webshop_lieferdienst') }}
