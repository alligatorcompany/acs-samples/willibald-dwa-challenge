{{ config (materialized='view') }}

select
    {{ dbt_utils.star(ref('stg_href_on_time_delivery')) }}
from {{ ref('stg_href_on_time_delivery') }}
