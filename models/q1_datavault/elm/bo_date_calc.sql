{{ config(materialized='table') }}
with
    date_spine as (

  {{ dbt_utils.date_spine(
      start_date="to_date('01/01/2022', 'mm/dd/yyyy')",
      datepart="day",
      end_date="to_date('01/01/2023', 'mm/dd/yyyy')"
     )
  }}
    )

select
    date_day,
    date_day as date_actual,
    day(date_day) as day_name,
    extract(month from date_day) as month_actual,
    extract(year from date_day) as year_actual,
    case
        when extract(month from date_day) = 1 then 'January'
        when extract(month from date_day) = 2 then 'February'
        when extract(month from date_day) = 3 then 'March'
        when extract(month from date_day) = 4 then 'April'
        when extract(month from date_day) = 5 then 'May'
        when extract(month from date_day) = 6 then 'June'
        when extract(month from date_day) = 7 then 'July'
        when extract(month from date_day) = 8 then 'August'
        when extract(month from date_day) = 9 then 'September'
        when extract(month from date_day) = 10 then 'October'
        when extract(month from date_day) = 11 then 'November'
        when extract(month from date_day) = 12 then 'December'
    end as month_name,
    week(date_day) as week_of_year
from date_spine
