{{ config(materialized='view')}}

select distinct
    vereinspartnerid,
    kaufdatum
from {{ ref('stg_rs_order') }}
