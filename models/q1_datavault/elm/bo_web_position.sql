SELECT
    {{ dbt_utils.star(from=source('businessobjects','order_item_s_web')) }}
{% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_ORDER_ITEM_S_WEB') }}
{% else %}
FROM {{ source('businessobjects','order_item_s_web') }}
{% endif %}
WHERE FV_ORDER_ITEM = true
