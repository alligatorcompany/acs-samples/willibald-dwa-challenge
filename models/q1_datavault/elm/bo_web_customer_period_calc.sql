with
    raw as (
        select distinct lts_customer
        {% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_CUSTOMER_S_WEB_C_SCD2_H') }}
{% else %}
        from {{ source('businessobjects','customer_s_web_c_scd2_h') }}
        {% endif %}
        where lts_customer is not null
        order by lts_customer asc
    )

select
    lts_customer,
    row_number() over (order by lts_customer asc) as periode
from raw
