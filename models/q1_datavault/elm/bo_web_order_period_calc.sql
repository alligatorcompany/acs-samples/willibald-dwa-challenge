with
    raw as (
        select distinct flvc_order
        {% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_ORDER_S_WEB_C_SCD2_H') }}
{% else %}
        from {{ source('businessobjects','order_s_web_c_scd2_h') }}
        {% endif %}
        where flvc_order is not null
        order by flvc_order asc
    )

select
    flvc_order,
    row_number() over (order by flvc_order asc) as periode
from raw
