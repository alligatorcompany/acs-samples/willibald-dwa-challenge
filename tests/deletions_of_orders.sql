--Erst mit der dritten und letzten Periode relevant
with
    src as (
        select bestellungid
        from {{ source('webshop_periode_1','bestellung_periode_1') }}
        where
            bestellungid not in
            (
                select bestellungid
                from {{ source('webshop_periode_2','bestellung_periode_2') }}
            )
        union all
        select bestellungid
        from {{ source('webshop_periode_2','bestellung_periode_2') }}
        where
            bestellungid not in
            (
                select bestellungid
                from {{ source('webshop_periode_3','bestellung_periode_3') }}
            )
    ),

    dv as (
        select bestellungid
        from {{ ref('bo_web_order_period') }}
        where delete_flag = true
    ),

    src_minus_dv as (

        select bestellungid from src
        except
        select bestellungid from dv

    ),

    dv_minus_src as (

        select bestellungid from dv
        except
        select bestellungid from src

    ),

    unioned as (

        select
            'src_minus_dv' as which_diff,
            src_minus_dv.*
        from src_minus_dv
        union all
        select
            'dv_minus_src' as which_diff,
            dv_minus_src.*
        from dv_minus_src

    )

select * from unioned
