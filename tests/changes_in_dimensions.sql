--Erst ab der zweiten Periode relevant
with
    src as (
        select
            katid,
            name
        from {{ source('webshop_periode_1','produktkategorie_periode_1')}}
        where
            katid not in
            (
                select katid
                from {{ source('webshop_periode_2','produktkategorie_periode_2')}}
            )
        union all
        select
            katid,
            name
        from {{ source('webshop_periode_2','produktkategorie_periode_2')}}
        where
            katid not in
            (
                select katid
                from {{ source('webshop_periode_1','produktkategorie_periode_1')}}
            )
        union all
        select
            katid,
            name
        from {{ source('webshop_periode_3','produktkategorie_periode_3')}}
        where
            katid not in
            (
                select katid
                from {{ source('webshop_periode_1','produktkategorie_periode_1')}}
            )
    ),

    dv as (
        select
            katid,
            name
        from
            {{ source('businessobjects','product_category_s_web') }}
    ),

    src_minus_dv as (

        select
            katid,
            name
        from src
        except
        select
            katid,
            name
        from dv

    ),

    dv_minus_src as (

        select
            katid,
            name
        from dv
        except
        select
            katid,
            name
        from src

    ),

    unioned as (

        select
            'src_minus_dv' as which_diff,
            src_minus_dv.*
        from src_minus_dv
        union all
        select
            'dv_minus_src' as which_diff,
            dv_minus_src.*
        from dv_minus_src

    )

select * from unioned
