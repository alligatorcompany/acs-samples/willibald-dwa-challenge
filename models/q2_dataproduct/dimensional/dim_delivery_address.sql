/*
with order as (
	select
		*
	from {{ ref('bo_web_order') }}
)

with order_item as (
	select
		*
	from {{ ref('bo_web_position') }}
)
*/

with
    customer as (
        select *
        from {{ ref('bo_web_customer') }}
    ),

    club_partner as (
        select *
        from {{ ref('bo_web_club_partner') }}
    ),

    delivery as (
        select *
        from {{ ref('bo_web_delivery') }}
    ),

    delivery_address as (
        select *
        from {{ ref('bo_web_delivery_address') }}
    ),

    rs_order as (
        select *
        from {{ ref('bo_road_rs_order') }}
    ),

    ws_delivery_address_data as (
        select
            cast(delivery_address.delivery_address_h as varchar(2000000)) as delivery_address_h,
            cast(delivery_address.lieferadrid as varchar(2000000)) as lieferadrid,
            customer.kundeid as kundeid,
            cast(delivery.delivery_h as varchar(2000000)) as delivery_h,
            cast(delivery.bestellungid as varchar(2000000)) as bestellungid,
            cast(delivery.posid as varchar(2000000)) as posid,
            cast(delivery.lieferdatum as varchar(2000000)) as lieferdatum,
            customer.customer_h,
            customer.vereinspartnerid as vereinspartnerid
        from delivery_address
        inner join delivery
            on delivery_address.lieferadrid = delivery.lieferadrid
        inner join customer
            on delivery_address.kundeid = customer.kundeid
        -- Capture Deliveries which have no Club_Partner
        left outer join club_partner
            on customer.vereinspartnerid = club_partner.vereinspartnerid
    ),

    rs_delivery_address_data as (
        select
            cast(null as varchar(2000000)) as delivery_address_h,
            cast(null as varchar(2000000)) as lieferadrid,
            customer.kundeid as kundeid,
            cast(null as varchar(2000000)) as delivery_h,
            cast(rs_order.bestellungid as varchar(2000000)) as bestellungid,
            cast(
                row_number() over (partition by bestellungid order by bestellungid) as varchar(2000000))
                as posid,
            cast(null as varchar(2000000)) as lieferdatum,
            customer.customer_h,
            customer.vereinspartnerid as vereinspartnerid
        from rs_order
        -- Capture Deliveries which have no Customer
        left outer join customer
            on rs_order.kundeid = customer.kundeid
        -- Capture Deliveries which have no Club_Partner
        left outer join club_partner
            on rs_order.vereinspartnerid = club_partner.vereinspartnerid
    -- No delivery, which meins no join on delivery
    )

select
    'ws' as order_type,
    delivery_address_h,
    lieferadrid,
    kundeid,
    posid,
    delivery_h,
    bestellungid,
    lieferdatum,
    customer_h,
    vereinspartnerid
from ws_delivery_address_data
union all
select
    'rs' as order_type,
    delivery_address_h,
    lieferadrid,
    kundeid,
    delivery_h,
    bestellungid,
    posid,
    lieferdatum,
    customer_h,
    vereinspartnerid
from rs_delivery_address_data
