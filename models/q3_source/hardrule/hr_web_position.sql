{{ config (materialized='view') }}

select
    {{ dbt_utils.star(ref('stg_web_position')) }}
from {{ ref('stg_web_position') }}
