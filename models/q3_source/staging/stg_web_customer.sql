{% set source_name = 'webshop_periode_' ~ var('periode_tag','1') %}
{% set table_name = 'kunde_periode_' ~ var('periode_tag','1') %}

{{ config (materialized='view') }}

{% if var("external_tables") == false %}
-- Seeds
{% if var("scd2_test")=='periode_4'%}
    select
      {{ dbt_utils.star(source('webshop_periode_4', 'kunde_periode_4')) }}
    from {{ source('webshop_periode_4', 'kunde_periode_4') }}
  {% else %}
select
    {{ dbt_utils.star(source(source_name, table_name)) }}
from {{ source(source_name, table_name) }}
{% endif %}
{% else %}
-- External Tables
  select
    {{ dbt_utils.star(source('q3_ext', 'ext_stg_web_customer')) }}
  from {{ source('q3_ext', 'ext_stg_web_customer') }}
{% endif %}
