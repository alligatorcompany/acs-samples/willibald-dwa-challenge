{% set source_name = 'webshop_periode_' ~ var('periode_tag','1') %}
{% set table_name = 'lieferadresse_periode_' ~ var('periode_tag','1') %}

{{ config (materialized='view') }}

{% if var("external_tables") == false %}
-- Seeds
select
    {{ dbt_utils.star(source(source_name, table_name)) }}
from {{ source(source_name, table_name) }}
{% else %}
-- External Tables
  select
    {{ dbt_utils.star(source('q3_ext', 'ext_stg_web_delivery_address')) }}
  from {{ source('q3_ext', 'ext_stg_web_delivery_address') }}
{% endif %}
