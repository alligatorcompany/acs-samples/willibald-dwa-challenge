{% set source_name = 'webshop_periode_' ~ var('periode_tag','1') %}
{% set table_name = 'wohnort_periode_' ~ var('periode_tag','1') %}

{{ config (materialized='view') }}

{% if var("external_tables") == false %}
-- Seeds
select
      {{ dbt_utils.star(from= source(source_name, table_name),
      except=["ADRESSZUSATZ"]) }},
    cast(ADRESSZUSATZ as varchar(100)) as ADRESSZUSATZ
from {{ source(source_name, table_name) }}
{% else %}
-- External Tables
  select
    {{ dbt_utils.star(from= source('q3_ext', 'ext_stg_web_address_residence'),
    except=["ADRESSZUSATZ"]) }},
    cast(ADRESSZUSATZ as varchar(100)) as ADRESSZUSATZ
  from {{ source('q3_ext', 'ext_stg_web_address_residence') }}
{% endif %}
