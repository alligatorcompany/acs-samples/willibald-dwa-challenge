with
    raw as (

        select *
        from {{ source('businessobjects','customer_s_web_c_scd2_h') }}
        where kundeid in (

            select kundeid
            from {{ source('businessobjects','customer_s_web_c_scd2_h') }}
            group by kundeid
            having count(distinct geschlecht) > 1
        )
    ),

    raw_periode as (

        select distinct lts_customer
        from {{ source('businessobjects','customer_s_web_c_scd2_h') }}
        where lts_customer is not null
        order by lts_customer asc
    ),

    lts_periode as (

        select
            lts_customer,
            row_number() over (order by lts_customer asc) as periode
        from raw_periode
    ),

    join_periode as (

        select
            raw.*,
            lts_periode.periode
        from raw
        inner join lts_periode
            on raw.lts_customer = lts_periode.lts_customer
    ),

    compare as (

        select kundeid
        from join_periode
        group by kundeid
        having count(kundeid) != (select max(periode) from join_periode)
    ),

    extra_record as (

        select
            customer_h,
            customer_bk,
            kundeid,
            vereinspartnerid,
            customer_s_web_c_scd2_h_lt,
            vorname,
            customer_s_web_c_scd2_h_lt_end,
            name,
            customer_s_web_c_scd2_h_bklt,
            geschlecht,
            geburtsdatum,
            telefon,
            mobil,
            email,
            kreditkarte,
            gueltigbis,
            dls_customer,
            kkfirma,
            lts_customer,
            fv_customer,
            flvc_customer,
            max(periode) + 1 as periode
            --cast(max(periode)+1 as decimal(18,0)) as periode
        from join_periode
        where
            kundeid in (select kundeid from compare)
            and periode = (select max(periode) - 1 from join_periode) -- noqa: PRS
        group by
            customer_h,
            customer_bk,
            kundeid,
            vereinspartnerid,
            customer_s_web_c_scd2_h_lt,
            vorname,
            customer_s_web_c_scd2_h_lt_end,
            name,
            customer_s_web_c_scd2_h_bklt,
            geschlecht,
            geburtsdatum,
            telefon,
            mobil,
            email,
            kreditkarte,
            gueltigbis,
            dls_customer,
            kkfirma,
            lts_customer,
            fv_customer,
            flvc_customer,
            periode
    ),

    pre_final as (
        select * from join_periode
        union all
        select * from extra_record
    )

select
    c.customer_h,
    c.customer_bk,
    c.kundeid,
    c.vereinspartnerid,
    c.customer_s_web_c_scd2_h_lt,
    c.vorname,
    c.customer_s_web_c_scd2_h_lt_end,
    c.name,
    c.customer_s_web_c_scd2_h_bklt,
    c.geschlecht,
    c.geburtsdatum,
    c.telefon,
    c.mobil,
    c.email,
    c.kreditkarte,
    c.gueltigbis,
    c.dls_customer,
    c.kkfirma,
    c.fv_customer,
    c.flvc_customer,
    c.lts_customer,
    cp.periode
from {{ source('businessobjects','customer_s_web_c_scd2_h') }} as c
inner join {{ ref('bo_web_customer_period_calc') }} as cp
    on c.lts_customer = cp.lts_customer
where
    c.lts_customer is not null
    and c.kundeid is not null
union
select
    customer_h,
    customer_bk,
    kundeid,
    vereinspartnerid,
    customer_s_web_c_scd2_h_lt,
    vorname,
    customer_s_web_c_scd2_h_lt_end,
    name,
    customer_s_web_c_scd2_h_bklt,
    geschlecht,
    geburtsdatum,
    telefon,
    mobil,
    email,
    kreditkarte,
    gueltigbis,
    dls_customer,
    kkfirma,
    fv_customer,
    flvc_customer,
    lts_customer,
    periode
from pre_final
