{% set source_name = 'webshop_periode_' ~ var('periode_tag','1') %}
{% set table_name = 'vereinspartner_periode_' ~ var('periode_tag','1') %}

{{ config (materialized='view') }}

{% if var("external_tables") == false %}
-- Seeds
select
    {{ dbt_utils.star(source(source_name, table_name)) }}
from {{ source(source_name, table_name) }}
{% else %}
-- External Tables
  select
    {{ dbt_utils.star(source('q3_ext', 'ext_stg_web_customer')) }}
  from {{ source('q3_ext', 'ext_stg_web_customer') }}
{% endif %}
