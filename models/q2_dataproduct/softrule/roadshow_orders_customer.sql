with
    kunde as (
        select
            kundeid,
            kreditkarte,
            gueltigbis,
            kkfirma
        from {{ source('businessobjects','customer_s_dvb_hub') }}
    ),

    rs_bestellung as (
        select
            bestellungid,
            kundeid,
            kreditkarte,
            gueltigbis,
            kkfirma
        from {{ source('businessobjects','sale_s_dvb_hub') }}
    )

select
    rs_bestellung.bestellungid,
    kunde.kundeid
from kunde
inner join rs_bestellung
    on
        kunde.kreditkarte = rs_bestellung.kreditkarte
        and kunde.gueltigbis = rs_bestellung.gueltigbis
        and kunde.kkfirma = rs_bestellung.kkfirma
