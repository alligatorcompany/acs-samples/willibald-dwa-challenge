SELECT
    {{ dbt_utils.star(from=source('businessobjects','club_partner_s_web')) }}
{% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_CLUB_PARTNER_S_WEB') }}
{% else %}
FROM {{ source('businessobjects','club_partner_s_web') }}
{% endif %}
WHERE FV_CLUB_PARTNER = true
