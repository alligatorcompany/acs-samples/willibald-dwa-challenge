with
    src as (
        select distinct la.kundeid
        from {{ source('webshop_periode_1','lieferadresse_periode_1')}} as la
        where
            la.kundeid not in
            (
                select kundeid
                from {{ source('webshop_periode_1','kunde_periode_1')}}
            )
    ),

    dv as (
        select distinct da.kundeid
        from {{ source('businessobjects','delivery_address_s_web') }} as da
        where
            da.kundeid not in
            (
                select kundeid
                from {{ source('businessobjects','customer_s_web') }}
            )
    ),

    src_minus_dv as (

        select kundeid from src
        except
        select kundeid from dv

    ),

    dv_minus_src as (

        select kundeid from dv
        except
        select kundeid from src

    ),

    unioned as (

        select
            'src_minus_dv' as which_diff,
            src_minus_dv.*
        from src_minus_dv
        union all
        select
            'dv_minus_src' as which_diff,
            dv_minus_src.*
        from dv_minus_src

    )

select * from unioned
