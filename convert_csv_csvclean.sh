#!/bin/bash
rm /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/*/*.csv
for f_read in /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds_raw/*/*.csv
do 
    ##echo "$f_read"
    csvclean -d ';' $f_read > csvclean_log.txt
done
for f_read_out in /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds_raw/*/*out*.csv
do 
    if [[ $f_read_out == *"seeds_href_termintreue_periode_1"* ]]; then
        mv $f_read_out /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_href_termintreue_periode_1/
    elif [[ $f_read_out == *"seeds_href_termintreue_periode_2"* ]]; then
        mv $f_read_out /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_href_termintreue_periode_2/
    elif [[ $f_read_out == *"seeds_ref_produkt_typ"* ]]; then
        mv $f_read_out /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_ref_produkt_typ/
    elif [[ $f_read_out == *"seeds_roadshow_tag_1"* ]]; then
        mv $f_read_out /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_roadshow_tag_1/
    elif [[ $f_read_out == *"seeds_roadshow_tag_2"* ]]; then
        mv $f_read_out /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_roadshow_tag_2/
    elif [[ $f_read_out == *"seeds_roadshow_tag_3"* ]]; then
        mv $f_read_out /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_roadshow_tag_3/
    elif [[ $f_read_out == *"seeds_webshop_periode_1"* ]]; then
        mv $f_read_out /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_webshop_periode_1/
    elif [[ $f_read_out == *"seeds_webshop_periode_2"* ]]; then
        mv $f_read_out /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_webshop_periode_2/
    elif [[ $f_read_out == *"seeds_webshop_periode_3"* ]]; then
        mv $f_read_out /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_webshop_periode_3/
    fi
done
for file in /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/*/*.csv
do
   mv "$file" "${file/_out/}"
done
##rename per folder
##href_termintreue_periode_1
cd /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_href_termintreue_periode_1/
for f_rename in *.csv;
    do
        ##echo "$f_rename"
        ##mv "$f_rename" periode_1_$f_rename
        mv "$f_rename" "${f_rename%.*}_periode_1.${f_rename##*.}"
    done
##href_termintreue_periode_2
cd /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_href_termintreue_periode_2/
for f_rename in *.csv;
    do
        ##echo "$f_rename"
        mv "$f_rename" "${f_rename%.*}_periode_2.${f_rename##*.}"
    done
##roadshow_tag_1
cd /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_roadshow_tag_1/
for f_rename in *.csv;
    do
        ##echo "$f_rename"
        mv "$f_rename" "${f_rename%.*}_tag_1.${f_rename##*.}"
    done
##roadshow_tag_2
cd /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_roadshow_tag_2/
for f_rename in *.csv;
    do
        ##echo "$f_rename"
        mv "$f_rename" "${f_rename%.*}_tag_2.${f_rename##*.}"
    done
##roadshow_tag_3
cd /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_roadshow_tag_3/
for f_rename in *.csv;
    do
        ##echo "$f_rename"
        mv "$f_rename" "${f_rename%.*}_tag_3.${f_rename##*.}"
    done
##periode_1
cd /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_webshop_periode_1/
for f_rename in *.csv;
    do
        ##echo "$f_rename"
        mv "$f_rename" "${f_rename%.*}_periode_1.${f_rename##*.}"
    done
##periode_2
cd /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_webshop_periode_2/
for f_rename in *.csv;
    do
        ##echo "$f_rename"
        mv "$f_rename" "${f_rename%.*}_periode_2.${f_rename##*.}"
    done
##periode_3
cd /Users/niklaslorenz/DBT_Projekte/willibald-dwa-challenge/seeds/seeds_webshop_periode_3/
for f_rename in *.csv;
    do
        ##echo "$f_rename"
        mv "$f_rename" "${f_rename%.*}_periode_3.${f_rename##*.}"
    done