{{ config (materialized='view') }}

select
    {{ dbt_utils.star(ref('stg_web_customer')) }}
from {{ ref('stg_web_customer') }}
