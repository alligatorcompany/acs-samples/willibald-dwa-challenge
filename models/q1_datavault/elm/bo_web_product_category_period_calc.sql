with
    raw as (
        select distinct flvc_product_category
        {% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_PRODUCT_CATEGORY_S_WEB_C_SCD2_H') }}
{% else %}
        from {{ source('businessobjects','product_category_s_web_c_scd2_h') }}
        {% endif %}
        where flvc_product_category is not null
        order by flvc_product_category asc
    )

select
    flvc_product_category,
    row_number() over (order by flvc_product_category asc) as periode
from raw
