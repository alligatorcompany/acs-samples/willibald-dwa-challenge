from dataclasses import dataclass
from typing import List, Dict
import snowflake.connector
import yaml
import os
import subprocess
from dotenv import load_dotenv
import concurrent.futures

@dataclass
class Fact:
    account: str
    role: str
    db: str
    schema: str
    user: str
    password: str
    fact: str
    tables: str
    attributes: Dict
    target_dir: str


def execute_sc(fact: Fact):
    try:
        os.makedirs(fact.target_dir, exist_ok = True)
        with open(f"{fact.target_dir}{fact.fact}.yaml", 'w') as file:
            yaml.dump(fact.attributes, file, default_flow_style=False)
        args = ["--command=schema", 
                "--info-level=standard", 
                f"--output-file={fact.fact}.png",
                "--output-format=png", 
                f"--url=\"jdbc:snowflake://{fact.account}.snowflakecomputing.com/?role={fact.role}&db={fact.db}&schema={fact.schema}\"", 
                f"--user={fact.user}", 
                f"--password={fact.password}", 
                f"--attributes-file={fact.fact}.yaml", 
                f"--tables=\"{fact.tables}\"",
                f"--schemas=\".*{str.upper(fact.db)}.*\..*{str.upper(fact.schema)}.*\""]
        full_command = ["sc"] + list(args)
        subprocess.run(full_command, check=True, cwd=f"{fact.target_dir}")
        print(".", end="")
    except Exception as e:
            raise e

@dataclass
class TableReference:
    catalog: str
    schema: str
    name: str

@dataclass
class WeakAssociation:
    name: str
    referenced_table: TableReference
    referencing_table: TableReference
    column_references: Dict[str, str]

@dataclass
class Database:
    name: str
    weak_associations: List[WeakAssociation]

load_dotenv()

# Snowflake connection parameters
SNOWFLAKE_ACCOUNT = os.environ.get('SNOWFLAKE_ACCOUNT') 
SNOWFLAKE_USER = os.environ.get('SNOWFLAKE_USER')
SNOWFLAKE_PASSWORD = os.environ.get('SNOWFLAKE_PASS')
SNOWFLAKE_WAREHOUSE = os.environ.get('SNOWFLAKE_WH')
SNOWFLAKE_DATABASE = os.environ.get('SNOWFLAKE_DB')
SNOWFLAKE_SCHEMA = os.environ.get('SNOWFLAKE_SCHEMA')
SNOWFLAKE_ROLE = os.environ.get('SNOWFLAKE_ROLE')
target_dir = os.environ.get('TARGET_DIR', 'assets/dm/')
# Connect to Snowflake
conn = snowflake.connector.connect(
    user=SNOWFLAKE_USER,
    password=SNOWFLAKE_PASSWORD,
    account=SNOWFLAKE_ACCOUNT,
    role=SNOWFLAKE_ROLE,
    warehouse=SNOWFLAKE_WAREHOUSE,
    database=SNOWFLAKE_DATABASE,
    schema=SNOWFLAKE_SCHEMA
)

# SQL Query to fetch data
# ACCESSLAYER durch DB_SCHEMA aus .env ersetzen
sql_query = f"""
with facts as (
    select table_name as fakt_name,column_name as fk_name
    from 
    information_schema.columns 
    where
    table_name like 'FAKT_%' and table_schema='{SNOWFLAKE_SCHEMA}'
    and column_name like '%_SK'
),
dims as (
    select table_name as dim_name,column_name as pk_name,JAROWINKLER_SIMILARITY(dim_name,pk_name) as similarity, similarity>70 as is_pk
    from 
    information_schema.columns 
    where
    (table_name like 'DIM_%') and table_schema='{SNOWFLAKE_SCHEMA}'
    and column_name like '%_SK'
),
rels as (
    select 
        fakt_name, fk_name, dim_name, pk_name, JAROWINKLER_SIMILARITY(facts.fk_name , dims.pk_name) as similarity, is_pk
    from facts left join dims on JAROWINKLER_SIMILARITY(facts.fk_name , dims.pk_name) > 75
),
rels_rank as (
    select
        fakt_name, fk_name, dim_name, pk_name, similarity, is_pk,
        fk_name || '_2_' || pk_name as rel_name,
        rank() over (partition by fakt_name, fk_name order by similarity desc) as ranking
    from rels
)
select * from rels_rank where ranking=1
order by fakt_name, fk_name, ranking desc;
"""

# Fetch data from Snowflake
cur = conn.cursor()
cur.execute(sql_query)
rows = cur.fetchall()
cur.close()
conn.close()

facts = list(set(row[0] for row in rows))
tasks = list()
for fact in facts:
    # Convert fetched data to your dataclass structures (modify this based on your query results)
    facts = list(set(row[0] for row in rows))
    fact_rows = [row for row in rows if row[0] == fact]
    data = Database(
        name=f"{SNOWFLAKE_DATABASE}",
        weak_associations=[
            WeakAssociation(
                name=row[6],  # Assuming the first column is 'name'
                referenced_table=TableReference(catalog=f"{SNOWFLAKE_DATABASE}", schema=f"{SNOWFLAKE_SCHEMA}", name=row[2]),
                referencing_table=TableReference(catalog=f"{SNOWFLAKE_DATABASE}", schema=f"{SNOWFLAKE_SCHEMA}", name=row[0]),
                column_references={
                    row[1]: row[3]  # Modify this based on your query results
                }
            ) for row in fact_rows
        ]
    )

    # Convert the dataclass instance to a dictionary
    data_dict = {
        "name": data.name,
        "weak-associations": [{
            "name": assoc.name,
            "referenced-table": assoc.referenced_table.__dict__,
            "referencing-table": assoc.referencing_table.__dict__,
            "column-references": assoc.column_references
        } for assoc in data.weak_associations]
    }

    tables = list(set(row[2] for row in fact_rows))
    tables.append(fact)
    formated_tables = '.*|.*'.join(tables)
    tables_parameter = f".*{formated_tables}.*"
    tasks.append(Fact(f"{SNOWFLAKE_ACCOUNT}", 
                      f"{SNOWFLAKE_ROLE}", 
                      f"{SNOWFLAKE_DATABASE}",
                      f"{SNOWFLAKE_SCHEMA}",
                      f"{SNOWFLAKE_USER}", 
                      f"{SNOWFLAKE_PASSWORD}", 
                      fact, 
                      tables_parameter, 
                      data_dict, target_dir))

with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
    results = list(executor.map(execute_sc, tasks))
    for result in results:
        print(result)
