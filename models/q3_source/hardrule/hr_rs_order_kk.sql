{{ config (materialized='view') }}

WITH
    sale_data AS (
        SELECT
            bestellungid,
            vereinspartnerid,
            max(kreditkarte) AS kreditkarte,
            max(gueltigbis) AS gueltigbis,
            max(kkfirma) AS kkfirma
        FROM {{ ref('stg_rs_order') }}
        GROUP BY bestellungid, vereinspartnerid
    )

SELECT * FROM sale_data
