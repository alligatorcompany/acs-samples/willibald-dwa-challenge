SELECT
    {{ dbt_utils.star(from=source('businessobjects','on_time_delivery_s_dvb_hub')) }}
{% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_ON_TIME_DELIVERY_S_DVB_HUB') }}
{% else %}
FROM {{ source('businessobjects','on_time_delivery_s_dvb_hub') }}
{% endif %}
WHERE FV_ON_TIME_DELIVERY = true
