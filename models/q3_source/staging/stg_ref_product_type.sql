{{ config (materialized='view') }}

{% if var("external_tables") == false %}
-- Seeds
select
    {{ dbt_utils.star(source('ref_produkt_typ', 'ref_produkt_typ')) }}
from {{ source('ref_produkt_typ', 'ref_produkt_typ') }}
{% else %}
-- External Tables
  select
    {{ dbt_utils.star(source('q3_ext', 'ext_stg_ref_product_typ')) }}
  from {{ source('q3_ext', 'ext_stg_ref_product_typ') }}
{% endif %}
