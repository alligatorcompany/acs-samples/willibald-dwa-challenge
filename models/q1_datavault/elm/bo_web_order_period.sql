select
    o.bestellungid as bestellungid,
    o.kundeid,
    o.allglieferadrid,
    o.bestelldatum,
    o.wunschdatum,
    o.rabatt,
    op.periode,
    coalesce(o.fv_order = false, false)
        as delete_flag
        {% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_ORDER_S_WEB_C_SCD2_H') }}
{% else %}
from
    {{ source('businessobjects','order_s_web_c_scd2_h') }}
        {% endif %}
        as o
inner join {{ ref('bo_web_order_period_calc') }} as op
    on o.flvc_order = op.flvc_order
where o.bestellungid is not null
