{{ config (materialized='view') }}

{% if var("external_tables") == false %}
-- Seeds
{% if var("periode_tag")=='1' %}
----------------Periode1----------------
select
    {{ dbt_utils.star(from=source('webshop_periode_1','bestellung_periode_1')) }},
    1 as periode,
    false as delete_flag
from {{ source('webshop_periode_1','bestellung_periode_1') }}
{% elif var("periode_tag")=='2' %}
    ----------------Periode2----------------
    select 
        {{ dbt_utils.star(from=source('webshop_periode_1','bestellung_periode_1')) }},
        1 as periode,
        false as delete_flag
    from {{ source('webshop_periode_1','bestellung_periode_1') }}
    union
    select 
        {{ dbt_utils.star(from=source('webshop_periode_2','bestellung_periode_2')) }},
        2 as periode,
        false as delete_flag
    from {{ source('webshop_periode_2','bestellung_periode_2') }}
    where bestellungid not in 
    (select bestellungid from {{ source('webshop_periode_1','bestellung_periode_1') }} )
    union
    select 
        {{ dbt_utils.star(from=source('webshop_periode_1','bestellung_periode_1')) }},
        2 as periode,
        true as delete_flag
    from {{ source('webshop_periode_1','bestellung_periode_1') }}
    where bestellungid not in
    (select bestellungid from {{ source('webshop_periode_2','bestellung_periode_2') }} )
    {% else %}
    ----------------Periode3----------------
    select 
        {{ dbt_utils.star(from=source('webshop_periode_1','bestellung_periode_1')) }},
        1 as periode,
        false as delete_flag
    from {{ source('webshop_periode_1','bestellung_periode_1') }}
    union
    select 
        {{ dbt_utils.star(from=source('webshop_periode_2','bestellung_periode_2')) }},
        2 as periode,
        false as delete_flag
    from {{ source('webshop_periode_2','bestellung_periode_2') }}
    where bestellungid not in 
    (select bestellungid from {{ source('webshop_periode_1','bestellung_periode_1') }} )
    union
    select 
        {{ dbt_utils.star(from=source('webshop_periode_3','bestellung_periode_3')) }},
        3 as periode,
        false as delete_flag
    from {{ source('webshop_periode_3','bestellung_periode_3') }}
    where bestellungid not in 
    (select bestellungid from {{ source('webshop_periode_2','bestellung_periode_2') }} )
    union
    select 
        {{ dbt_utils.star(from=source('webshop_periode_1','bestellung_periode_1')) }},
        2 as periode,
        true as delete_flag
    from {{ source('webshop_periode_1','bestellung_periode_1') }}
    where bestellungid not in 
    (select bestellungid from {{ source('webshop_periode_2','bestellung_periode_2') }} )
    union
    select 
        {{ dbt_utils.star(from=source('webshop_periode_2','bestellung_periode_2')) }},
        3 as periode,
        true as delete_flag
    from {{ source('webshop_periode_2','bestellung_periode_2') }}
    where bestellungid not in
    (select bestellungid from {{ source('webshop_periode_3','bestellung_periode_3') }} )
    {% endif %}
{% else %}
-- External Tables
  select
    {{ dbt_utils.star(source('q3_ext', 'ext_stg_web_order')) }}
  from {{ source('q3_ext', 'ext_stg_web_order') }}
{% endif %}
