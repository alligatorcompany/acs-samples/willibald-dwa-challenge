{{ config (materialized='view') }}

WITH
    sale_item_data AS (
        SELECT
            bestellungid,
            kundeid,
            vereinspartnerid,
            produktid,
            max(kaufdatum) AS kaufdatum,
            max(menge) AS menge,
            max(preis) AS preis,
            max(rabatt) AS rabatt
        FROM {{ ref('stg_rs_order') }}
        GROUP BY bestellungid, kundeid, vereinspartnerid, produktid
    )

SELECT * FROM sale_item_data
