SELECT
    {{ dbt_utils.star(from=source('businessobjects','customer_s_web')) }}
{% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_CUSTOMER_S_WEB') }}
{% else %}
FROM {{ source('businessobjects','customer_s_web') }}
{% endif %}
WHERE FV_CUSTOMER = true
