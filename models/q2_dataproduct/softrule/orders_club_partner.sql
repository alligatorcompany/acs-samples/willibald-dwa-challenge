with
    bestellung as (

        select
            customer_h,
            bestellungid
        from {{ source('businessobjects','order_s_dvb_hub') }}
        where customer_h is not null

    ),

    kunde as (

        select
            customer_h,
            vereinspartnerid
        from {{ source('businessobjects','customer_s_dvb_hub') }}
        where vereinspartnerid is not null

    )

select
    kunde.vereinspartnerid,
    bestellung.bestellungid
from
    bestellung inner join kunde
    on bestellung.customer_h = kunde.customer_h
