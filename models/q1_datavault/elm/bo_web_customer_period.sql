select
	{{ dbt_utils.star(from= source('businessobjects','customer_s_web_c_scd2_h'),
    except=["LTS_CUSTOMER"]) }},
    c.lts_customer,
    cp.periode
    {% if target.name == 'docs' %}
from {{ ref('BUSINESSOBJECTS_CUSTOMER_S_WEB_C_SCD2_H') }}
{% else %}
from
    {{ source('businessobjects','customer_s_web_c_scd2_h') }}
    {% endif %}
        as c
inner join {{ ref('bo_web_customer_period_calc') }} as cp
    on c.lts_customer = cp.lts_customer
where
    c.lts_customer is not null
    and c.kundeid is not null
